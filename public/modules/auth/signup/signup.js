app.controller('SignUpCtrl', function ($scope, $rootScope, server, utils, $location){


    $scope.formModel = {
        firstInput  : "",
        username  : "",
        password  : "",
        email  : ""
      };

    $scope.signUpPageRender = {
        form: true,
        message: false
    };
    $scope.message = {
        body: "",
        title: ""
    }

    $scope.setMessage = function(options){
        $scope.message.body = options.body || '';
        $scope.message.title = options.title || '';
    }

  
    $scope.sendSignUpRequest = function(formModel){
        var msgOpt = {
            time : 5000
          , isClosable : true
        }
        , success = function(success){
            var message = success.data.message;
            if(success.data.success){
              var userData = success.data.data;
            //   $scope.login(userData);
            //   msgOpt.type = 'success'
              // utils.info.alert.show(message, msgOpt);

              $scope.setMessage({
                title: 'Su cuenta fue creada con éxito',
                body: 'Aguarde. Estamos iniciando sesión para usted.'
            })
            $scope.signUpPageRender.form = false;
            $scope.signUpPageRender.message = true;


            setTimeout(function(){ 
                $scope.sendAuthenticationRequest();
            }, 2000); 
            // display message 
            // "YOUR USER WAS CREATED" // "WE ARE STARTING YOUR SESSION"


            //   $scope.formModel[credentialFieldToSend] = $scope.formModel.firstInput;
            //   server.signIn(false, { data : { 
            //                           "password": $scope.formModel.password, 
            //                           "email" : $scope.formModel.email
            //                       }
            //                     }, {}).then(success, error);


            //   $location.path('/signin');
            }
            else{
              msgOpt.type = 'warning'
              utils.info.alert.show(message, msgOpt);
            }
          // utils.loading.hide();
        }
        ,   error = function(error){
            var message = error.data.message;
            msgOpt.type = 'danger'
            utils.info.alert.show(message, msgOpt);
          // utils.loading.hide();
        };
        console.log($scope.formModel)
        var credentialFieldToSend = ($scope.formModel.firstInput.indexOf('@') == -1)?'username':'email';
        //ToDo: replace with ifValidMail() function
  
        $scope.formModel[credentialFieldToSend] = $scope.formModel.firstInput;
        server.signUp(false, { data : { 
                                "name": $scope.formModel.name, 
                                "surname": $scope.formModel.surname, 
                                "password": $scope.formModel.password, 
                                "email" : $scope.formModel.email,
                                "type" : 'customer'
                            }
                          }, {}).then(success, error);
        // utils.loading.show();
    };



    $scope.sendAuthenticationRequest = function(formModel){
        var msgOpt = {
            time : 5000
          , isClosable : true
        }
        , success = function(success){
            var message = success.data.message;
            if(success.data.success){
              var userData = success.data.data;
              $scope.login(userData);
              msgOpt.type = 'success'
              // utils.info.alert.show(message, msgOpt);

                // setTimeout(function(){ 
                //       
                // }, 3000);

                // setTimeout(function(){ 
                //     alert("Hello"); 
                //     $rootScope.$broadcast('redirect-page', { data: { path : '/'} });  
                // }, 3000); 

                $location.path('/');
            }
            else{
              msgOpt.type = 'warning'
              utils.info.alert.show(message, msgOpt);
            }
          // utils.loading.hide();
        }
        ,   error = function(error){
            var message = error.data.message;
            msgOpt.type = 'danger'
            utils.info.alert.show(message, msgOpt);
          // utils.loading.hide();
        };
        console.log($scope.formModel)
        var credentialFieldToSend = ($scope.formModel.firstInput.indexOf('@') == -1)?'username':'email';
        //ToDo: replace with ifValidMail() function
  
        $scope.formModel[credentialFieldToSend] = $scope.formModel.firstInput;
        server.signIn(false, { data : { 
                                "password": $scope.formModel.password, 
                                "email" : $scope.formModel.email
                            }
                          }, {}).then(success, error);
      };


})
