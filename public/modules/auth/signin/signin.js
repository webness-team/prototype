app.controller('SignInCtrl', function ($scope, server, utils, $location){

    $scope.formModel = {
      firstInput  : "",
      username  : "",
      password  : "",
      email  : ""
    };

    $scope.sendAuthenticationRequest = function(formModel){
      var msgOpt = {
          time : 5000
        , isClosable : true
      }
      , success = function(success){
          var message = success.data.message;
          if(success.data.success){
            var userData = success.data.data;
            $scope.login(userData);
            msgOpt.type = 'success'
            // utils.info.alert.show(message, msgOpt);
            $location.path('/');
          }
          else{
            msgOpt.type = 'warning'
            utils.info.alert.show(message, msgOpt);
          }
        // utils.loading.hide();
      }
      ,   error = function(error){
          var message = error.data.message;
          msgOpt.type = 'danger'
          utils.info.alert.show(message, msgOpt);
        // utils.loading.hide();
      };
      console.log($scope.formModel)
      var credentialFieldToSend = ($scope.formModel.firstInput.indexOf('@') == -1)?'username':'email';
      //ToDo: replace with ifValidMail() function

      $scope.formModel[credentialFieldToSend] = $scope.formModel.firstInput;
      server.signIn(false, { data : { 
                              "username": $scope.formModel.username, 
                              "password": $scope.formModel.password, 
                              "email" : $scope.formModel.email
                          }
                        }, {}).then(success, error);
      // utils.loading.show();
    };

})
