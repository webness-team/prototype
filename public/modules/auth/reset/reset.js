app.controller('ResetPasswordCtrl', function ($scope, server, utils, $routeParams){


    $scope.resetPasswordToken = $routeParams.resetPasswordToken || '';


    $scope.userId = '';
    
    $scope.formModel = {
      passwordOne  : "",
      passwordTwo  : ""
    };

    $scope.message = {
        title : '',
        body: ''
    };
    $scope.renderResetPassword = {
        message: false,
        form: false,
        linkReset: false,
        linkBrowse: false
    };

    $scope.setMessage = function(options){
        $scope.message.body = options.body || '';
        $scope.message.title = options.title || '';
    }

    $scope.checkResetPasswordTokenValidity = function(token){
        // var isValidToken = true;
        // if(token==''){
        //     isValidToken = false;
        //     return isValidToken;
        // }
        // // then, should call a service to validate token 
        // // and get a the user id from inside of it
        // var arrToken = token.split('.');
        // arrToken.forEach(function(itm, idx){
        //     if(idx!=3 && itm.length!=3){
        //         isValidToken = false;
        //     }
        // })
        // return isValidToken;
    };

    $scope.updatePassword = function(formModel){
      var msgOpt = {
          time : 5000
        , isClosable : true
      }
      , success = function(success){
          var message = success.data.message;
          if(success.data.success){

            $scope.setMessage({
                title: 'Su contraseña fue reestablecida',
                body: '¡Por favor anótala en un lugar seguro!'
            })
            $scope.renderResetPassword.form = false;
            $scope.renderResetPassword.linkBrowse = true;
            $scope.renderResetPassword.message = true;
          }
          else{
            // msgOpt.type = 'warning'
            // utils.info.alert.show(message, msgOpt);
            $scope.setMessage({
                title: 'Ups. Ocurrió un problema',
                body: 'No hemos logrado reestablecer tu contraseña. Intenta nuevamente.'
            })
            $scope.renderResetPassword.form = true;
            $scope.renderResetPassword.message = true;
          }
        // utils.loading.hide();
      }
      ,   error = function(error){
          var message = error.data.message;
          msgOpt.type = 'danger'
          utils.info.alert.show(message, msgOpt);
        // utils.loading.hide();
      };
      //ToDo: replace with ifValidMail() function

      if($scope.formModel.passwordOne != $scope.formModel.passwordTwo){
        $scope.setMessage({
            title: '',
            body: 'Las contraseñas no coinciden. Reintenta.'
        })
        $scope.renderResetPassword.form = true;
        $scope.renderResetPassword.message = true;
      }
      else{
          server.updatePassword(false, { data : { 
                  "password" : $scope.formModel.passwordOne,
                  "token" : $scope.resetPasswordToken
              }
            }, {}).then(success, error);
          };
    };

    $scope.init = function(){
        $scope.renderResetPassword.form = true;
        // if($scope.checkResetPasswordTokenValidity($scope.resetPasswordToken)){
        //     $scope.userId = $scope.getUserIdromToken($scope.resetPasswordToken);
        //     $scope.renderResetPassword.form = true;
        // }
        // else{
        //     $scope.message = {
        //         title : 'Algo salió mal',
        //         body: 'Debes obtener un código válido para poder reestablecer tu contraseña. Intenta nuevamente.'
        //     };
        //     $scope.renderResetPassword.form = false;
        //     $scope.renderResetPassword.message = true;
        //     $scope.renderResetPassword.linkReset = true;
        // };        
    };

    $scope.init();


})



