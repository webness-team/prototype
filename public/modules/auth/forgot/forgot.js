app.controller('ForgotPasswordCtrl', function ($scope, server, utils, $location){

    $scope.formModel = {
      email  : ""
    };

    $scope.forgotPageRender = {
        form: true,
        message: false
    };

    $scope.message = {
        body: "",
        title: ""
    }

    $scope.setMessage = function(options){
        $scope.message.body = options.body || '';
        $scope.message.title = options.title || '';
    }

    $scope.sendPasswordUpdateRequest = function(formModel){
      var msgOpt = {
          time : 5000
        , isClosable : true
      }
      , success = function(success){
          var message = success.data.message;
          if(success.data.success){
            var userData = success.data.data;
            // msgOpt.type = 'success'
            // utils.info.alert.show(message, msgOpt);
            $scope.setMessage({
                title: 'Revisa tu correo',
                body: 'Te hemos enviado un enlace para restablecer tu contraseña.'
            })
            $scope.forgotPageRender.form = false;
            $scope.forgotPageRender.message = true;

          }
          else{
            msgOpt.type = 'warning'
            utils.info.alert.show(message, msgOpt);

            $scope.setMessage({
                title: 'Ups. Ocurrió un problema',
                body: 'No hemos logrado enviarte un enlace para restablecer tu contraseña. Inténtalo de nuevo.'
            })
            $scope.forgotPageRender.form = true;
            $scope.forgotPageRender.message = true;
          }
        // utils.loading.hide();
      }
      ,   error = function(error){
          var message = error.data.message;
          msgOpt.type = 'danger'
          utils.info.alert.show(message, msgOpt);
        // utils.loading.hide();
      };
      //ToDo: replace with ifValidMail() function

      server.forgotPassword(false, { data : { 
                              "email" : $scope.formModel.email
                          }
                        }, {}).then(success, error);
      // utils.loading.show();
    };

})
