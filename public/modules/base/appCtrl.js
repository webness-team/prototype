app.controller('AppCtrl', function ($scope, $rootScope, $timeout, server, session, utils, $location, $routeParams, activitiesService, usersService, meetingsService, locationsService, $element, calendar, overallService, subheaderService, tabsService){

  
    $scope.myKeyDownFunction = function(event) {
      if(event.keyCode === 27) {
        $scope.onOverallClick('off');
        // angular.element('#search-bar-input').blur(); // blur
        // https://stackoverflow.com/questions/38474975/angular-tapping-outside-of-input-does-not-trigger-blur
      }
    }
    // https://stackoverflow.com/questions/17457024/angular-ng-click-with-call-to-a-controller-function-not-working

    $scope.goTo = function(options){
      $location.path(options.path)
    }

    $scope.render = {
      global : {
          alert : {
            // message: '',
            // show: false
          }
        , sidebar : {
            isVisible  : true // pullElementsOverWhenSidebar
          , isActive   : true // pullElementsOverWhenSidebar
        }
        ,
        // overall : {} ,
        dashboard: {
          isVisible : false
        }
        ,
        calendar: {
          isActive: false
        }
        ,
        header: {
          height: '40px',
          isVisible : true
        }
        ,
        subheader: {
          height: '40px',
          isVisible : false
        }
        ,
        content: {
          bg : {
            color : '#fff'
          }
        }
        ,
        searchbar : {
          results: false
        }
      }
    };

    $scope.tabsConfig = tabsService.getConfig();
    $rootScope.$on('updated-tabs-config', function(event, args) {
      $scope.tabsConfig = tabsService.getConfig();
      console.log($scope.tabsService, 'tabsService');
    });

    $scope.subheaderConfig = subheaderService.getConfig();
    $rootScope.$on('updated-subheader-config', function(event, args) {
      $scope.subheaderConfig = subheaderService.getConfig();
    });

    $scope.overallConfig = overallService.getConfig();

    $scope.renderCalendar = {
      show : false
    }
    $scope.getUserById = function(id){
      return usersService.getUserById(id)
    }
    $scope.getLocationById = function(id){
      return locationsService.getLocationById(id)
    }
    $scope.getMeetingById = function(id){
      return meetingsService.getMeetingById(id)
    }


    $scope.selectedLocation =  locationsService.getSelectedLocation();
    $rootScope.$on('update-selected-location', function(event, args) {
      $scope.selectedLocation =  locationsService.getSelectedLocation();
      $rootScope.$broadcast('init-map');
    });
    $scope.setSelectedLocation = function(id){
      locationsService.setSelectedLocationId(id);
      $rootScope.$broadcast('update-selected-location');
    }

    $scope.selectedUser =  usersService.getSelectedUser();
    $rootScope.$on('update-selected-user', function(event, args) {
      $scope.selectedUser =  usersService.getSelectedUser();
    });
    $scope.setSelectedUser = function(id){
      usersService.setSelectedUserId(id);
      $rootScope.$broadcast('update-selected-user');
    }



    $scope.selectedActivity = {}
    $rootScope.$on('update-selected-activity', function(event, args) {
      $scope.selectedActivity =  activitiesService.getSelectedActivity();
    });
    $scope.setSelectedActivity = function(id){
      activitiesService.setSelectedActivityId(id);
      $rootScope.$broadcast('update-selected-activity');
    }


    $scope.selectedDay = calendar.getSelectedDay();
    $scope.setSelectedDay = function(day){
      calendar.setSelectedDay(day);
    }
    $rootScope.$on('updated-calendar-selected-day', function(event, args) {
        $scope.selectedDay = calendar.getSelectedDay();
    });


    $rootScope.$on('update-content-bg', function(event, args) {
      $scope.render.global.content.bg.color = args.bg.color;
    });
    

    $scope.openSidebar = function(){
      $scope.render.global.sidebar.isVisible = ! $scope.render.global.sidebar.isVisible; // $scope.render.global.sidebar.isActive = true;
    }
    $scope.createFriendlyUrl = function(value){
      return value == undefined ? '' : value.replace(/[^a-z0-9_]+/gi, '-').replace(/^-|-$/g, '').toLowerCase();
    }
    
    $scope.onOverallClick = function(options){
      if(options.switch=='off') {
        overallService.setDefaultConfig()
        return
      }
      overallService.handleVisibility('oposite');
    }

    $scope.handleSearchBarResultsGui = function(param){
      $rootScope.$broadcast('search-bar-results-gui-'+param);
    }

    $scope.openActivityDetailCrud = function(){
      $rootScope.$broadcast('activity-detail-crud-gui'+'-on');
    }
    $scope.openLocationDetailCrudGui = function(){
      $rootScope.$broadcast('location-detail-crud-gui'+'-on');
    }
    $scope.openUserDetailCrudGui = function(){
      $rootScope.$broadcast('user-detail-crud-gui'+'-on');
    }
    $scope.handleCalendarCrudGui = function(options){
      if($scope.render.global.calendar.isActive){
        $scope.render.global.calendar.isActive = !$scope.render.global.calendar.isActive;
        let previuosPath = calendar.getPreviuosRoute();
        $scope.goTo({path: previuosPath })
      }
      else{
        $scope.render.global.calendar.isActive = !$scope.render.global.calendar.isActive;
        calendar.setPreviuosRoute($rootScope.currentPath)
        $scope.goTo({path: $rootScope.currentPath+'/calendar'})
      }
    }
    $scope.openActivityAddGui = function(options){
      $rootScope.$broadcast('activity-add-gui'+'-on');
    }
    $scope.openLocationAddGui = function(options){
      $rootScope.$broadcast('location-add-gui'+'-on');
    }
    $scope.openUserAddGui = function(options){
      $rootScope.$broadcast('user-add-gui'+'-on');
    }
    $scope.openMeetingAddGui = function(options){
      $rootScope.$broadcast('meeting-add-gui'+'-on');
    }

    


    $rootScope.$on('updated-overall-config', function(event, args) {
      $scope.overallConfig = overallService.getConfig();
    });
    $rootScope.$on('search-bar-results-gui-on', function(event, args) {
      overallService.setBgTransparent(true)
      overallService.setComponentOn('searchBarResultsGui');
      $scope.onOverallClick({gui: 'searchBarResultsGui'});
    });
    $rootScope.$on('search-bar-results-gui-off', function(event, args) {
      overallService.setComponentOff('searchBarResultsGui');
    });
    $rootScope.$on('activity-add-gui-on', function(event, args) {
      overallService.setComponentOn('activityAddGui');
      $scope.onOverallClick({gui: 'activityAddGui'});
    });
    $rootScope.$on('activity-add-gui-off', function(event, args) {
      overallService.setComponentOff('activityAddGui');
    });
    $rootScope.$on('user-add-gui-on', function(event, args) {
      overallService.setComponentOn('userAddGui');
      $scope.onOverallClick({gui: 'userAddGui'});
    });
    $rootScope.$on('user-add-gui-off', function(event, args) {
      overallService.setComponentOff('userAddGui');
    });
    $rootScope.$on('meeting-add-gui-on', function(event, args) {
      overallService.setBgTransparent(true)
      overallService.setComponentOn('meetingAddGui');
      $scope.onOverallClick({gui: 'meetingAddGui'});
    });
    $rootScope.$on('meeting-add-gui-off', function(event, args) {
      // console.log('meeting-add-gui-off in progress')
      calendar.setSelectedDay({});
      overallService.setComponentOff('meetingAddGui');
    });
    $rootScope.$on('location-add-gui-on', function(event, args) {
      overallService.setComponentOn('locationAddGui');
      $scope.onOverallClick({gui: 'locationAddGui'});
    });
    $rootScope.$on('location-add-gui-off', function(event, args) {
      overallService.setComponentOff('locationAddGui');
    });
    $rootScope.$on('activity-detail-crud-gui-on', function(event, args) {
      overallService.setComponentOn('activityDetailCrudGui');
      $scope.onOverallClick({gui: 'activityDetailCrudGui'});
    });
    $rootScope.$on('activity-detail-crud-gui-off', function(event, args) {
      overallService.setComponentOff('activityDetailCrudGui');
    });
    $rootScope.$on('location-detail-crud-gui-on', function(event, args) {
      overallService.setComponentOn('locationDetailCrudGui');
      $scope.onOverallClick({gui: 'locationDetailCrudGui'});
    });
    $rootScope.$on('location-detail-crud-gui-off', function(event, args) {
      overallService.setComponentOff('locationDetailCrudGui');
    });
    $rootScope.$on('user-detail-crud-gui-on', function(event, args) {
      overallService.setComponentOn('userDetailCrudGui');
      $scope.onOverallClick({gui: 'userDetailCrudGui'});
    });
    $rootScope.$on('user-detail-crud-gui-off', function(event, args) {
      overallService.setComponentOff('userDetailCrudGui');
    });

    $scope.login          = function(user){
      utils.session.open(user);
    };
    $scope.logout         = function(){
      $scope.render.global.subheader.isVisible = false;
      utils.session.close();
    };
    $scope.getUserType = function(){
      return utils.session.getUser().user_type;
    }


    // services calls - starts ------------------
    // activities
    $scope.getActivities = function(options, callback){
      $scope.makeHiddenElementsAppear = false;
      var success = function(success){
        var result = [];
        if(success.data && success.data.data && success.data.data.items){
          result = success.data.data.items;
        }
        else{
          //ToDo: handle service error
        };
        activitiesService.persistActivities([]);
        activitiesService.setActivities(result);
        $rootScope.$broadcast('activities-updated');
        if(callback){
          callback();
        }
      }
      ,   error = function(error){
        activitiesService.setActivities([]);
        $rootScope.$broadcast('activities-updated');
        //ToDo: handle global error
      };
      // server.getActivities(options, success, error);
      var opts = {params: {id : "all"}};
      server.getActivities(false, opts, {}).then(success, error);
      $rootScope.$broadcast('updating-activities');
      // $rootScope.$broadcast('start-ng-progress', {});
    };
    // locations
    $scope.getLocations = function(idsArr, callback){
      var success = function(success){
        var result = [];
        if(success.data && success.data.data && success.data.data.items){
          result = success.data.data.items;
        }
        else{
          //ToDo: handle service error
        };
        locationsService.persistLocations([]);
        locationsService.setLocations(result);
        $rootScope.$broadcast('locations-updated');
        if(callback){
          callback();
        }
      }
      ,   error = function(error){
        locationsService.setLocations([]);
        $rootScope.$broadcast('locations-updated');
        //ToDo: handle global error
      };
      server.getLocations(false, {params: {id : "all"}}, {}).then(success, error);
    };
    // meetings
    $scope.getMeetings = function(idsArr, callback){
      var success = function(success){
        var result = [];
        if(success.data && success.data.data && success.data.data.items){
          result = success.data.data.items;
        }
        else{
          //ToDo: handle service error
        };
        meetingsService.persistMeetings([]);
        meetingsService.setMeetings(result);
        $rootScope.$broadcast('meetings-updated');
        if(callback){
          callback();
        }
      }
      ,   error = function(error){
        meetingsService.setMeetings([]);
        $rootScope.$broadcast('meetings-updated');
        //ToDo: handle global error
      };
      server.getMeetings(false, {params: {id : "all"}}, {}).then(success, error);
    };
    // users
    $scope.getUsers = function(idsArr, callback){
      var success = function(success){
        var result = [];
        if(success.data && success.data.data && success.data.data.items){
          result = success.data.data.items;
        }
        else{
          //ToDo: handle service error
        };
        usersService.persistUsers([]);
        usersService.setUsers(result);
        $rootScope.$broadcast('users-updated');
        if(callback){
          callback();
        }
      }
      ,   error = function(error){
        usersService.setUsers([]);
        $rootScope.$broadcast('users-updated');
        //ToDo: handle global error
      };
      server.getUsers(false, {params: {id : "all"}}, {}).then(success, error);
    };
    $scope.addActivities = function(input){
      var success = function(success){
        var result = [];
        if(success.data && success.data.data && success.data.data.items){
          result = success.data.data;
        }
        else{
          //ToDo: handle service error
          console.log(success)
        };
        // usersService.persistUsers([]);
        // usersService.setUsers(result);
        $rootScope.$broadcast('activity-added');
      }
      ,   error = function(error){
        // usersService.setUsers([]);
        // $rootScope.$broadcast('users-updated');
        //ToDo: handle global error
      };
      server.addActivities(false, { data : input
      }, {}).then(success, error);
    };
    $rootScope.$on('activity-added', function(event, args) {
      $scope.getActivities();
    });
    $scope.addLocations = function(input){
      var success = function(success){
        var result = [];
        if(success.data && success.data.data){
          result = success.data.data;
          // var activityUpdated = activitiesService.getSelectedActivity().locations.push(success.data.data.location_id)
          // activitiesService.setActivities([activityUpdated])
          $rootScope.$broadcast('location-added');
        }
        else{
          //ToDo: handle service error
          console.log(success)
        };
      }
      ,   error = function(error){
        // usersService.setUsers([]);
        // $rootScope.$broadcast('users-updated');
        //ToDo: handle global error
      };
      server.addLocations(false, { data : input
      }, {}).then(success, error);
    };
    // sendInvitationToNewMembers
    $scope.sendInvitationToNewMembers = function(input){
      var success = function(success){
        var result = [];
        if(success.data && success.data.data){
          result = success.data.data;
          $rootScope.$broadcast('new-member-invitation-sent');
          $rootScope.$broadcast('members-added');
        }
        else{
          //ToDo: handle service error
          console.log(success)
        };
      }
      ,   error = function(error){
        // usersService.setUsers([]);
        // $rootScope.$broadcast('users-updated');
        //ToDo: handle global error
      };
      server.sendInvitationToNewMembers(false, { data : input
      }, {}).then(success, error);      
    }
    $scope.addMeetings = function(input){
      var success = function(success){
        var result = [];
        if(success.data && success.data.data && success.data.data.items){
          result = success.data.data;
        }
        else{
          //ToDo: handle service error
          console.log(success)
        };
        // usersService.persistUsers([]);
        // usersService.setUsers(result);
        $rootScope.$broadcast('meetings-added');
      }
      ,   error = function(error){
        // usersService.setUsers([]);
        // $rootScope.$broadcast('users-updated');
        //ToDo: handle global error
      };
      server.addMeetings(false, { data : input
      }, {}).then(success, error);
    };
    $rootScope.$on('meetings-added', function(event, args) {
      $scope.getMeetings();
    });
    // services calls - ends --------------------


    // toDo --- starts
      
      // when activities updated, should trigger:
      // => getMeetings
      // => getLocations
      // => getUsers

      // when meetings updated, should trigger:
      // => getActivities

      // when locations updated, should trigger:
      // => getActivities

      // when members updated, should trigger:
      // => getActivities

    // toDo --- ends 



    $scope.addLocationTemplateDefault = {
      "title": "",
      "owner": utils.session.getUser().id,
      "description": "",
      "friendlyUrl": "",
      "address": "",
      "imgUrl": "",
      "coors": { "lat": "", "lng": "" },
      "activity_id": "",
      "meeting_id": "", 
    }
    $scope.addLocationTemplate = {
      "title": "",
      "owner": utils.session.getUser().id,
      "description": "",
      "friendlyUrl": "",
      "address": "",
      "imgUrl": "",
      "coors": { "lat": "", "lng": "" },
      "activity_id": "",
      "meeting_id": "",
      // "members": [""], // "locations": [""], // "meetings": [""],
    }
    $scope.addNewLocation = function(options){
      if($scope.addLocationTemplate.title==""){
        return false;
      }
      $scope.addLocationTemplate.activity_id = activitiesService.getSelectedActivity().id || false;
      $scope.addLocations($scope.addLocationTemplate);
      $scope.onOverallClick({switch: 'off'})
    }

    $scope.inviteNewMembersTemplate = {
      "email": "",
      "name": "",
      "surname": "",
      "username": "",
      "activity_id": "",
      "activity_friendly_url": "",
      "meeting_id": "",
      "profile_pic": "",
    }
    $scope.inviteNewMembers = function(options){
      if($scope.inviteNewMembersTemplate.email==""){
        return false;
      }
      // if([CHECK_IF_USER_IS_NOT_ALREADY_A_MEMBER]){
      //   return false;
      // }
      $scope.inviteNewMembersTemplate.activity_id = activitiesService.getSelectedActivity().id || false;
      $scope.inviteNewMembersTemplate.activity_friendly_url = activitiesService.getSelectedActivity().friendlyUrl || false;
      $scope.inviteNewMembersTemplate.meeting_id = (meetingsService.getSelectedMeeting())?meetingsService.getSelectedMeeting().id:false;
      $scope.sendInvitationToNewMembers($scope.inviteNewMembersTemplate);
      $scope.onOverallClick({switch: 'off'})
    }



    $scope.addMeetingTemplate = {
      "title": "",
      "description": "",
      "activity_id": "",
      ///
      "meeting_number": "",
      "location_id": "",
      
      "date": "",

      "time_scheduled": "",
      
      "organizers": "",
      "attendants": [],
      "is_active": true
    }
    $scope.addNewMeeting = function(options){
      options = options || {};
      if($scope.addMeetingTemplate.title==""){
        return false;
      }
      $scope.addMeetingTemplate.activity_id = activitiesService.getSelectedActivity().id || false;
      $scope.addMeetingTemplate.location_id = options.location_id || false;
      $scope.addMeetingTemplate.date = $scope.selectedDay.dayNumber + '/' + $scope.selectedDay.monthNumber + '/' + $scope.selectedDay.year ,
      $scope.addMeetingTemplate.time_scheduled = options.time_scheduled || false;
      $scope.addMeetings($scope.addMeetingTemplate);
      $scope.onOverallClick({switch: 'off'})
    }


    // $rootScope.$on('activities-updated', function(event, args) {
    //   // var oldActivitiesLength = $scope.activitiesArr.length;
    //   // $scope.activitiesArr = activitiesService.getActivities() || [];
    // });
    $rootScope.$on('updating-activities', function(event, args) {
      $scope.render.global.dashboard.isVisible = false;
      // var oldActivitiesLength = $scope.activitiesArr.length;
      // $scope.activitiesArr = activitiesService.getActivities() || [];
    });
    $rootScope.$on('unauthorized-response-handler', function(event, args) {
        console.log('unauthorized-response-handler => loggin out ...')
        $scope.logout();
    });

    $rootScope.$on('handling-subheader-visibility', function(event, args) {
        subheaderService.handleVisibility(args.data.value, 'event')      
    });

    $rootScope.$on('session-lost', function(event, args) {
        //ToDo: inform the user that session was lost
        overallService.setDefaultConfig();
        
        meetingsService.persistMeetings([]);
        meetingsService.setMeetings([]);
        locationsService.setLocations([]);
        activitiesService.setActivities([]);
        usersService.setUsers([]);
        tabsService.setCurrentTab({index: {number: 0} });
        console.log('session was lost. please log in again')
        $rootScope.$broadcast('redirect-page', { data: { path : '/signin'} });
    });
    $rootScope.$on('session-updated', function(event, args) {
        $rootScope.$broadcast('update-header');
    });
  
    $rootScope.$on('message-updated', function(event, args) {
      var info = utils.info.get();
      $scope.render.global.alert = info;
    });
    $rootScope.$on('redirect-page', function(event, args) {
      var data = args.data;
      $location.path(data.path);
    });

  })


  .filter('trustAsResourceUrl', ['$sce', function ($sce) {
      return function (val) {
          return $sce.trustAsResourceUrl(val);
      };
  }])

  app.filter('orderByTheme', function() {
    return function(items, field, reverse) {
      var filtered = [];
      angular.forEach(items, function(item) {
        filtered.pu0(item);
      });
      filtered.sort(function (a, b) {
        return (a[field] > b[field] ? 1 : -1);
      });
      if(reverse) filtered.reverse();
      return filtered;
    };
  });


      // $scope.getActivityByFriendlyUrl = function(){
    //   var success = function(success){
    //     var result = [];
    //     $scope.activityRender.page = true;
    //     if(success.data && success.data.data && success.data.data.items){
    //       $scope.activity = success.data.data.items[0];
    //     }
    //     else{
    //       //ToDo: handle service error
    //     };
    //   }
    //   ,   error = function(error){
    //     activitiesService.setActivities([]);
    //     $rootScope.$broadcast('activities-updated');
    //     //ToDo: handle global error
    //   };
    //   var query = {};
    //   if ($scope.friendlyUrl!=undefined){
    //     query = {"friendlyUrl": $scope.friendlyUrl};
    //   }
    //   server.getActivities(false, { params: query }, {}).then(success, error);
    // };
