app.controller('ActivityCtrl', function ($scope, activitiesService, locationsService, meetingsService, usersService, $routeParams, $rootScope, server, $location, subheaderService, tabsService){

  $scope.activityRender = {
    page : true
  }

  $rootScope.$broadcast('update-content-bg', { bg : { color: '#fff' }});


  $scope.friendlyUrl = $routeParams.friendlyUrl;
  $scope.urlTab = $routeParams.tab || false;
  if($scope.urlTab){
    tabsService.setCurrentTab({ listName: 'activities', index: false, id: $scope.urlTab});
  }else{
    let currentTab = tabsService.getCurrentTab('activities');
    if (!currentTab) {
      tabsService.setCurrentTab({ listName: 'activities', index: { number : 1 }, id: false});
    }
  }
  $scope.tabs = $scope.tabsConfig.activities.list; // ['overview', 'locations', 'members', 'credits', 'payments', 'settings']
  $scope.tabsService = tabsService;
  $scope.changeTab = function(index){
    tabsService.setCurrentTab({ listName: 'activities', index: { number : index}, id: false}); // ;
  }
  $scope.meetingsArr = [];
  $scope.usersArr = [];
  $scope.locationsArr = [];
  $scope.activity = {};

  $scope.updateMeetingsLocally = function(){
    let meetings = meetingsService.getMeetings()
    ,   result  = [];
    meetings.forEach(function(itm, idx){
      if($scope.activity.meetings.includes(itm.id)){
        result.push(itm)
      }
    })
    $scope.meetingsArr = result;
    // console.log($scope.meetingsArr)
  }
  $scope.updateUsersLocally = function(){
        // $scope.usersArr = usersService.getUsers();
    // console.log($scope.usersArr)
    let users = usersService.getUsers()
    ,   result  = [];
    users.forEach(function(itm, idx){
      if($scope.activity.members.includes(itm.id)){
        result.push(itm)
      }
    })
    $scope.usersArr = result;
  }
  $scope.updateLocationsLocally = function(){
    let locations = locationsService.getLocations()
    ,   result  = [];
    locations.forEach(function(itm, idx){
      if($scope.activity.locations.includes(itm.id)){
        result.push(itm)
      }
    })
    $scope.locationsArr = result;
    // $scope.locationsArr = locationsService.getLocations();
    // console.log($scope.locationsArr)
  }

  $rootScope.$on('meetings-updated', function(event, args) {
    $scope.updateMeetingsLocally()
  });
  $rootScope.$on('users-updated', function(event, args) {
    $scope.updateUsersLocally()
  });
  $rootScope.$on('locations-updated', function(event, args) {
    $scope.updateLocationsLocally()
  });
  $rootScope.$on('location-added', function(event, args) {
    var getActivities = function(){
      $scope.getActivities();
    };
    $scope.getLocations([], getActivities);
  });
  $rootScope.$on('members-added', function(event, args) {
    var getActivities = function(){
      $scope.getActivities();
    };
    $scope.getUsers([], getActivities);
  });
  $rootScope.$on('activities-updated', function(event, args) {
    $scope.activity = activitiesService.getActivitiesByFriendlyUrl($scope.friendlyUrl);
    $scope.updateLocationsLocally();
    $scope.updateMeetingsLocally();
    $scope.updateUsersLocally();
    // $scope.getMeetings({idsArr: $scope.activity.meetings })
    // $scope.getLocations({idsArr: $scope.activity.locations })
    // $scope.getUsers({idsArr: $scope.activity.members })
  });

  
  // $scope.activity.titleRename = $scope.activity.title;
  // $scope.renameActivity = function(param){
  //   $scope.activity.title = $scope.activity.titleRename;
  // }

  $scope.init = function(){
    // $scope.activity = $scope.getActivityByFriendlyUrl($routeParams.friendlyUrl) || {}
    $scope.activity = activitiesService.getActivitiesByFriendlyUrl($scope.friendlyUrl);
    $scope.setSelectedActivity($scope.activity.id);
    
    subheaderService.setData($scope.activity)


    // $scope.getMeetings({idsArr: $scope.activity.meetings })
    // $scope.getLocations({idsArr: $scope.activity.locations })
    // $scope.getUsers({idsArr: $scope.activity.members })

    $scope.updateLocationsLocally();
    $scope.updateMeetingsLocally();
    $scope.updateUsersLocally();

    $scope.getMeetings();
    $scope.getLocations();
    $scope.getUsers();



  }


  $scope.init();




  

})
