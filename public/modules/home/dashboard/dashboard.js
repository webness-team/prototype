app.controller('DashboardCtrl', function (activitiesService, $scope, $rootScope, server, utils, $location, $routeParams, subheaderService){

  $scope.activitiesRender       = {};

  $rootScope.$on('activities-updated', function(event, args) {
    $scope.activitiesArr = activitiesService.getActivities();
    $scope.render.global.dashboard.isVisible = true;
  });

  // $scope.render.global.subheader.isVisible = false;
  // subheaderService.handleVisibility(false, 'dashboard');


  // $scope.activitiesArr = activitiesService.getActivities();
  // $scope.getActivities();

  $scope.palette = [
    {
      name: 'royalblue',
    },
    {
      name: 'mediumpurple',
    },
    {
      name: 'darkkhaki',
    },
    {
      name: 'burlywood',
    },
    {
      name: 'darkseagreen',
    },
    {
      name: 'crimson',
    },
  ];
  $scope.selectedColor = $scope.palette[0];

  $scope.selectColor = function(idx){
    $scope.selectedColor = $scope.palette[idx];
  }

  $scope.addActivityTemplate = {
    "title": "",
    "friendlyUrl": "",


    "imgs": { "main": "" },
    "background_color": "",

    "owner": utils.session.getUser().id,
    "members": [""],
    "locations": [""],
    "meetings": [""],
  }
  $scope.addNewActivity = function(options){
    if($scope.addActivityTemplate.title==""){ return false };
    $scope.addActivityTemplate.background_color = options.background_color;
    $scope.addActivityTemplate.friendlyUrl = $scope.createFriendlyUrl($scope.addActivityTemplate.title)
    $scope.addActivityTemplate.date = new Date();
    $scope.addActivities($scope.addActivityTemplate);
    $scope.onOverallClick({switch: 'off'})
  };
  
  $scope.updateActivitiesLocally = function(){
    // let locations = locationsService.getLocations()
    // ,   result  = [];
    // locations.forEach(function(itm, idx){
    //   if($scope.activity.locations.includes(itm.id)){
    //     result.push(itm)
    //   }
    // })
    // $scope.locationsArr = result;
  }
  $scope.init = function(){
    // $scope.updateActivitiesLocally();
    $scope.activitiesArr = activitiesService.getActivities();
    $scope.getActivities();
  }
  $scope.init();
})
