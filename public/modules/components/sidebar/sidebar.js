app.controller('SidebarCtrl', function ($scope, $rootScope, utils){

  $scope.handleSidebar = false;
  $scope.handleSidebarFromSidebar = function(){
    var sidebarIsActive = $scope.render.global.sidebar.isActive;
    if(sidebarIsActive){
      $scope.render.global.sidebar.isVisible = !$scope.render.global.sidebar.isVisible;
    }
    $scope.render.global.pullOverWhenSidebar = !$scope.render.global.pullOverWhenSidebar ;
  }

})
