app.controller('HeaderCtrl', function ($scope, $rootScope, utils, searchService){

  var defaultUserData = {
       email : "---"
    ,  name : "---"
    ,  role : "peer"
    ,  surname : "---"
    ,  username : "---"
  };

  $scope.searchResults = [];
  $scope.searchText = { input : null };

  $scope.userData = utils.session.getUser();
  $rootScope.$on('updated-search-bar-results', function(event, args) {
    $scope.searchResults = searchService.returnResults();
  });

  $scope.headerRender       = {
    login   : {
      loginBtn : true
    }
    ,
    user    : {
        settingsBtn : false
      , data : defaultUserData
    },
    searchBar: {
      expanded : false,
      results : false
    }
  };

  $scope.showSearchBarResults = function(flag){
    $scope.headerRender.searchBar.results = flag;
  }

  $scope.onSearchBarFocus = function(){
    // $scope.headerRender.searchBar.expanded = !($scope.headerRender.searchBar.expanded);
    // $scope.showSearchBarResults
  }

  $scope.onSearchBarBlur = function(){
    $scope.headerRender.searchBar.expanded = !($scope.headerRender.searchBar.expanded);
  }

  $scope.clearSearch = function(){
    $scope.searchResults = [];
    $scope.searchText = { input : null };
  }

  $scope.search = function() {
    if (!$scope.overallConfig.components.searchBarResultsGui) {
      $scope.handleSearchBarResultsGui('on');
    }
    searchService.find($scope.searchText.input);
  }

  $scope.handleSearchBar = function(param){
    $scope.headerRender.searchBar.expanded = !$scope.headerRender.searchBar.expanded;
  }

  $scope.updateUserInfo = function(data){
    $scope.headerRender.user.data = data;
    $scope.userData = data;
  };

  $scope.updateHeader = function(){
    var activeSession = ($rootScope.activeSession)?true:false
      , userData      = defaultUserData;
    if(activeSession){
      $scope.headerRender.login.loginBtn     = false;
      $scope.headerRender.user.settingsBtn   = true;

      $scope.render.global.sidebar.isVisible   = false;
      $scope.render.global.sidebar.isActive    = true;


      userData = utils.session.getUser();
    }
    else{
      $scope.headerRender.login.loginBtn     = true;
      $scope.headerRender.user.settingsBtn   = false;

      $scope.render.global.sidebar.isVisible  = false;
      $scope.render.global.sidebar.isActive   = false;
    };
    $scope.updateUserInfo(userData)
  };

  $scope.$on('update-header', function(event, args) {
    $scope.updateHeader()
  });

  $scope.init = function(){
    $scope.updateHeader();
  };
  $scope.init();

})
