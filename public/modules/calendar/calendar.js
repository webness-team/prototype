app.controller('CalendarCtrl', function ($scope, $rootScope, utils, calendar, subheaderService, meetingsService){
    // $scope.render.global.subheader.isVisible = true;
    // subheaderService.handleVisibility(true, 'calendar');
    $rootScope.$broadcast('update-content-bg', { bg : { color: '#026aa7' }});


    $scope.calendarBack = function(){
        calendar.setOtherMonth(-1);
        $scope.currentWeeksInMonth  = calendar.getWeeksArr();
        $scope.$apply();
    }
    $scope.calendarNext = function(){
        calendar.setOtherMonth(1);
        $scope.currentWeeksInMonth  = calendar.getWeeksArr();
        $scope.$apply();
    }

    $scope.currentWeeksInMonth  = calendar.getWeeksArr();

    $scope.getCurrentWeeksInMonth = function(){
        return $scope.currentWeeksInMonth;
    }



    $scope.getActivityMeetingsByDate = function(day){
        var activityMeetings = meetingsService.getMeetingsById($scope.selectedActivity.meetings);
        var result = [];
        var formatedDate = day.dayNumber + "/" + day.monthNumber  + "/" + day.year;
        activityMeetings.forEach(function(itm, idx){
            if(itm.date==formatedDate){
                result.push(itm)
            }
        })
        // console.log(activityMeetings)
        // console.log($scope.selectedActivity.meetings)
        // console.log(formatedDate)
        // console.log(result)
        return result;
    }
});
  
