angular.module("app.session", [])

.factory("session", ["$rootScope", "persistence", 
     "overallService", "meetingsService", "locationsService", "activitiesService", "usersService", "tabsService",
    function($rootScope, persistence, server,
    overallService, meetingsService, locationsService, activitiesService, usersService, tabsService){
  $rootScope.activeSession    = persistence.get("webness.app.activeSession") || false;

  var setData         = function(userData){
      userData.googleApiCode = "";
      persistence.set("webness.app.userData", userData)
      persistence.set("webness.app.activeSession", true)
      $rootScope.activeSession = true;
      $rootScope.$broadcast('session-updated');
      console.log('welcome. session was started')
  }
  , getUser           = function(){
    var user = persistence.get("webness.app.userData") || {};
    return user;
  }
  , getHash           = function(){
    var hash = persistence.get("webness.app.userData") || {};
    hash = hash.token || {};
    return hash;
  }
  , defaultPermisology       = {
      feature1    : false
    , feature2    : false
    , feature3    : false
    , feature4    : false
  }
  // , permisology      = defaultPermisology
  , closeSession     = function(){
      persistence.remove("webness.app.userData");
      persistence.remove("webness.app.activeSession");
      persistence.remove("webness.app.activities");

      persistence.remove("webness.app.locations");
      persistence.remove("webness.app.meetings");
      persistence.remove("webness.app.users");
      persistence.remove("webness.app.tabs");

      $rootScope.activeSession = false;
      $rootScope.$broadcast('session-updated');
      $rootScope.$broadcast('session-lost');
      console.log('goodbye. session was closed')
  };

	return new function(){
		this.user 			= {
        set           : setData
      , get           : getUser
      , getHash       : getHash
    }
    this.close      = closeSession
	};

}]);
