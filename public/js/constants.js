angular.module('app.constants', [])

// .constant("serverPath", "http://localhost:5000/api/")
.constant("serverPath", "/api/")

.constant("endpoints", {
    signIn            : function(actions, verb, options, cachingOptions){
        var service             = "auth/sign_in"
        ,   action              = "post";
        return actions[action](service, options);
    }
    ,
    signUp            : function(actions, verb, options, cachingOptions){
        var service             = "auth/user"
        ,   action              = "post";
        return actions[action](service, options);
    }
    ,
    getUsers            : function(actions, verb, options, cachingOptions){
        var service             = "auth/user"
        ,   action              = "get";
        return actions[action](service, options);
    }
    ,
    updateUsers            : function(actions, verb, options, cachingOptions){
        var service             = "auth/user"
        ,   action              = "put";
        return actions[action](service, options);
    }
    ,
    disableUsers            : function(actions, verb, options, cachingOptions){
        var service             = "auth/user"
        ,   action              = "delete";
        return actions[action](service, options);
    }
    ,
    forgotPassword            : function(actions, verb, options, cachingOptions){
        var service             = "auth/user/forgot"
        ,   action              = "post";
        return actions[action](service, options);
    }
    ,
    updatePassword            : function(actions, verb, options, cachingOptions){
        var service             = "auth/user/password"
        ,   action              = "put";
        return actions[action](service, options);
    }
    ,    
    getActivities            : function(actions, verb, options, cachingOptions){
        var service             = "activities"
        ,   action              = "get";
        return actions[action](service, options);
    }
    ,    
    addActivities            : function(actions, verb, options, cachingOptions){
        var service             = "activities"
        ,   action              = "post";
        return actions[action](service, options);
    }
    ,    
    getLocations            : function(actions, verb, options, cachingOptions){
        var service             = "locations"
        ,   action              = "get";
        return actions[action](service, options);
    }
    ,   
    addLocations            : function(actions, verb, options, cachingOptions){
        var service             = "locations"
        ,   action              = "post";
        return actions[action](service, options);
    }
    ,
    addMeetings            : function(actions, verb, options, cachingOptions){
        var service             = "meetings"
        ,   action              = "post";
        return actions[action](service, options);
    }
    ,
    sendInvitationToNewMembers : function(actions, verb, options, cachingOptions){
        var service             = "auth/user/invite"
        ,   action              = "post";
        return actions[action](service, options);
    }
    ,    
    getMeetings            : function(actions, verb, options, cachingOptions){
        var service             = "meetings"
        ,   action              = "get";
        return actions[action](service, options);
    }
    ,    
    getUsers            : function(actions, verb, options, cachingOptions){
        var service             = "auth/user"
        ,   action              = "get";
        return actions[action](service, options);
    }
    // , testWithCaching       : function(action, opt, getPromise, cachingOpts){
    //     var service              = "test"
    //     , action                     = action || "get"
    //     , cachingOpts      = cachingOpts || {
    //         isForcedCall        : true
    //       , maxAge                  : 15*60000
    //       , isCrossSession  : false
    //     };
    //     cachingOpts.action  = action;

    //     if(caching.mustCallService(service, cachingOpts)){
    //         return actions[action](service, opt, getPromise, cachingOpts);
    //     }
    //     else{
    //         return caching.recoverData(service, opt, getPromise, cachingOpts)
    //     };
    // }
})

.constant("responseHandler", function(data, rootScope){
    // console.log("networkConfig => executing => customHandler as responseHandler")

    if(data.status===401){
        rootScope.$broadcast("unauthorized-response-handler")
    }

    if(data.status===403){
        // inform user what is happening
    }

    if(data.status===409){
        // inform user what is happening
    }

    if(data.status===500){
        // inform user what is happening
    }
    
})
