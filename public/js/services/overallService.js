angular.module("app.gui.overallService", [])
.service('overallService', function($rootScope, persistence) {
    var config = {
        bgTransparent: false,
        isVisible: false,
        components: {
            activityAddGui: false,
            userAddGui: false,
            locationAddGui: false,
            meetingAddGui: false,
            activityDetailCrudGui: false,
            userDetailCrudGui: false,
            locationDetailCrudGui: false,
            meetingDetailCrudGui: false,
            searchBarResultsGui: false,
          }
    }
    ,   setDefaultConfig = function(){
        config = {
            bgTransparent: false,
            isVisible: false,
            components: {
                activityAddGui: false,
                userAddGui: false,
                locationAddGui: false,
                meetingAddGui: false,
                activityDetailCrudGui: false,
                userDetailCrudGui: false,
                locationDetailCrudGui: false,
                meetingDetailCrudGui: false,
                searchBarResultsGui: false,
            }
         }
         $rootScope.$broadcast('updated-overall-config');
         setComponentsOff();
    }
    ,   getConfig = function(){
            return config;
    }
    ,   setBgTransparent = function(value){
            config.bgTransparent = value;
            $rootScope.$broadcast('updated-overall-config');
    }
    ,   setComponentOff = function(name){
        config.components[name] = false;
        $rootScope.$broadcast('updated-overall-config');
    }
    ,   setComponentOn = function(name){
        config.components[name] = true;
        $rootScope.$broadcast('updated-overall-config');
    }
    ,   handleVisibility = function(value){
        if (value=='oposite'){
            config.isVisible = !config.isVisible;
        }
        else{
            config.isVisible = value;
        }
        $rootScope.$broadcast('updated-overall-config');
    }
    ,   setComponentsOff = function(){
        $rootScope.$broadcast('activity-add-gui-off');
        $rootScope.$broadcast('location-add-gui-off');
        $rootScope.$broadcast('user-add-gui-off');
        $rootScope.$broadcast('meeting-add-gui-off');
        $rootScope.$broadcast('activity-detail-crud-gui-off');
        $rootScope.$broadcast('search-bar-results-gui-off');
        $rootScope.$broadcast('location-detail-crud-gui-off');
        $rootScope.$broadcast('user-detail-crud-gui-off');
    };
        return {
            getConfig: getConfig,
            setBgTransparent: setBgTransparent,
            setComponentOn: setComponentOn,
            setComponentOff: setComponentOff,
            handleVisibility: handleVisibility,
            setComponentsOff: setComponentsOff,
            setDefaultConfig: setDefaultConfig
        };
   }
);