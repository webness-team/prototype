angular.module("app.usersService", [])
.service('usersService', function($rootScope, persistence) {
    var selectedUserId = ""
    ,
        setUsers   = function(arr){
            var users = getUsers();
            arr.forEach(function(itm, idx){
                users.push(itm);
            })
            persistUsers(users)
    }
    // ,   getUsersByFriendlyUrl = function(friendlyUrl){
    //         var users = persistence.get("webness.app.users")
    //         ,   result;
    //         users.forEach(function(itm,idx){
    //             if(itm.friendlyUrl===friendlyUrl){
    //                 result = itm;
    //             }
    //         })
    //         return result;
    // }
    ,   persistUsers		  = function(users){
            persistence.set("webness.app.users", users)
    }
    ,
        getUsers				= function(){
            var users = persistence.get("webness.app.users") || [];
            return users;
    }
    ,   removeUsersById = function(id){
            var users = persistence.get("webness.app.users")
            users.forEach(function(itm, idx){
                if(itm.id===id){
                    delete users[idx];
                }
            })
            persistUsers(users);
    }
    ,
        getUserById = function(id){
        var users = persistence.get("webness.app.users") || [];
        var result;
        users.forEach(function(itm, idx){
            if(itm.id===id){
                result = users[idx];
            }
        })
        return result;
    }
    ,
    setSelectedUserId = function(id){
        selectedUserId = id
    }
    ,   getSelectedUser = function(id){
            var users = getUsers();
            var result = {};
            users.forEach(function(itm, idx){
                if (itm.id == selectedUserId) {
                    result = itm;
                }
            })
            return result;
    } 
        return {
            setSelectedUserId: setSelectedUserId,
            getSelectedUser: getSelectedUser,
            setUsers: setUsers,
            // getUsersByFriendlyUrl: getUsersByFriendlyUrl,
            persistUsers : persistUsers,
            getUsers: getUsers,
            removeUsersById: removeUsersById,
            getUserById: getUserById,
        };
   }
);
