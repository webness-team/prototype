angular.module("app.meetingsService", [])
.service('meetingsService', function($rootScope, persistence) {
    var selectedMeetingId = ""
    ,
        setMeetings   = function(arr){
            var meetings = getMeetings();
            arr.forEach(function(itm, idx){
                meetings.push(itm);
            })
            persistMeetings(meetings);
    }
    // ,   getUsersByFriendlyUrl = function(friendlyUrl){
    //         var users = persistence.get("webness.app.users")
    //         ,   result;
    //         users.forEach(function(itm,idx){
    //             if(itm.friendlyUrl===friendlyUrl){
    //                 result = itm;
    //             }
    //         })
    //         return result;
    // }
    ,   persistMeetings		  = function(meetings){
            persistence.set("webness.app.meetings", meetings)
    }
    ,
        getMeetings				= function(){
            var meetings = persistence.get("webness.app.meetings") || [];
            return meetings;
    }
    ,
        getMeetingsByDate				= function(date){
            // var meetings = persistence.get("webness.app.meetings") || [];
            // var result;
            // meetings.forEach(function(itm, idx){
            //     if(itm.id===id){
            //         result = meetings[idx];
            //     }
            // })
            // return result;
    }
    ,
        getMeetingsByActivity				= function(date){
            // var meetings = persistence.get("webness.app.meetings") || [];
            // var result;
            // meetings.forEach(function(itm, idx){
            //     if(itm.id===id){
            //         result = meetings[idx];
            //     }
            // })
            // return result;
}
    ,   removeMeetingsById = function(id){
            var meetings = persistence.get("webness.app.meetings")
            meetings.forEach(function(itm, idx){
                if(itm.id===id){
                    delete meetings[idx];
                }
            })
            persistMeetings(meetings);
    }
    ,
        getSelectedMeeting				= function(id){
            var meetings = getMeetings();
            var result = false;
            meetings.forEach(function(itm, idx){
                if (itm.id == selectedMeetingId) {
                    result = itm;
                }
            })
            return result;
    }
    ,
    getMeetingById = function(id){
        var meetings = persistence.get("webness.app.meetings") || [];
        var result;
        meetings.forEach(function(itm, idx){
            if(itm.id===id){
                result = meetings[idx];
            }
        })
        return result;
    }
    ,
    getMeetingsById = function(idArr){
        var meetings = persistence.get("webness.app.meetings") || []
        ,   result  = [];
        meetings.forEach(function(itm, idx){
          if(idArr.includes(itm.id)){
            result.push(itm)
          }
        })
        return result;
    };
        return {
            // getMeetingsByDate: getMeetingsByDate,
            // getMeetingsByActivity: getMeetingsByActivity,
            getMeetingById: getMeetingById,
            getMeetingsById: getMeetingsById,
            setMeetings: setMeetings,
            // getUsersByFriendlyUrl: getUsersByFriendlyUrl,
            persistMeetings : persistMeetings,
            getMeetings: getMeetings,
            removeMeetingsById: removeMeetingsById,
            getSelectedMeeting: getSelectedMeeting,
        };
   }
);