angular.module("app.activitiesService", [])
.service('activitiesService', function($rootScope, persistence) {
    var selectedActivityId = ""
    ,   setActivities   = function(arr){
            var activities = getActivities();
            arr.forEach(function(itm, idx){
                // var title = itm.title
                activities.push(itm);
            })
            persistActivities(activities)
    }
    ,   getActivitiesByFriendlyUrl = function(friendlyUrl){
            var activities = persistence.get("webness.app.activities")
            ,   result;
            activities.forEach(function(itm,idx){
                if(itm.friendlyUrl===friendlyUrl){
                    result = itm;
                }
            })
            return result;
    }
    ,   persistActivities		  = function(activities){
            persistence.set("webness.app.activities", activities)
    }
    ,
        getActivities				= function(){
            var activities = persistence.get("webness.app.activities") || [];
            return activities;
    }
    ,   removeActivityById = function(id){
            var activities = persistence.get("webness.app.activities")
            activities.forEach(function(itm, idx){
                if(itm.id===id){
                    delete activities[idx];
                }
            })
            persistActivities(activities);
    }
    ,
        setSelectedActivityId				= function(id){
            selectedActivityId = id
    }
    ,
        getSelectedActivity				= function(id){
            var activities = getActivities();
            var result = {};
            activities.forEach(function(itm, idx){
                if (itm.id == selectedActivityId) {
                    result = itm;
                }
            })
            return result;
        };
        return {
            setActivities: setActivities,
            getActivities: getActivities,
            persistActivities : persistActivities,
            getActivitiesByFriendlyUrl: getActivitiesByFriendlyUrl,
            removeActivityById: removeActivityById,
            setSelectedActivityId: setSelectedActivityId, 
            getSelectedActivity: getSelectedActivity, 
        };
   }
);