angular.module("app.searchService", [])
.service('searchService', function($rootScope, persistence) {
    var results = []
    ,   currentSearchTermObj = {}
    , 
        getResults = function() {
            var number = Math.floor(Math.random() * 13) + 1;
            var resultObj = {
            1 : [ 
                // locations obj
                // meetings obj
                // users obj
                // payments obj
                'Joaquín', 'Santiago', 'Jorge', 'Martina', 'Joel', 'Miguel', 'Noe', 'Jaime', 'Ter', 'Fer', 'Eddie'],
            2 : [ 'Perro', 'Caballo', 'Ágila', 'Gato', 'Ballena', 'Camello', 'Serpiente', 'Tiburón', 'Araña', 'Toro', 'Gallina'],
            3 : [ 'Galicia', 'Paris', 'Marbella', 'New York', 'Quito', 'Córdoba', 'Queens', 'Manchester', 'Antofagasta', 'La Paz', 'Santa Cruz'],
            4 : [ 'Afganistán','Albania','Alemania','Andorra','Angola','Antigua','Arabia','Argelia','Argentina','Armenia','Australia','Austria','Azerbaiyán','Bahamas','Bangladés','Barbados','Baréin','Bélgica','Belice','Benín','Bielorrusia','Birmania','Bolivia','Bosnia','Botsuana','Brasil','Brunéi','Bulgaria','Burkina','Burundi','Bután', ],
            5 : [ 'monitor', 'bread', 'sticky note', 'drill press', 'bow', 'tooth picks', 'boom box', 'lamp shade', 'paper', 'mirror', 'perfume', 'fridge', 'leg warmers', 'cookie jar', 'wallet',],
            6 : [ 'clothes', 'spring', 'rubber band', 'bread', 'window', 'candle', 'beef', 'teddies', 'canvas', 'shovel', 'thermometer', 'magnet', 'cinder block', 'screw', 'drill press',],
            7 : [ 'key chain', 'carrots', 'sidewalk', 'speakers', 'soda can', 'clothes', 'stop sign', 'window', 'lamp', 'floor', 'door', 'tire swing', 'twezzers', 'spring', 'vase',],
            8 : [ 'rubber band', 'ice cube tray', 'radio', 'stockings', 'needle', 'soda can', 'thread', 'sticky note', 'cup', 'towel', 'computer', 'drill press', 'shampoo', 'money', 'boom box',],
            9 : [ 'plastic fork', 'air freshener', 'bracelet', 'box', 'zipper', 'wallet', 'piano', 'stop sign', 'eye liner', 'tomato', 'candy wrapper', 'lotion', 'soda can', 'wagon', 'hair brush',],
            10 : ['washing machine','checkbook','key chain','ice cube tray', 'greeting card', 'deodorant','soy sauce packet','chapter book','button','couch','spoon','bed','wallet','tomato','soap',],
            11 : [],
            12 : [],
            13 : [],
            }
            return resultObj[number];
    }
    ,   find = function(searchTermObj){
            searchTermObj = searchTermObj
            $rootScope.$broadcast('updated-search-bar-results');
    }
    ,   getSearchTerm = function(){
        return currentSearchTermObj;
    }
    // setUsers   = function(arr){
    //         var users = getUsers();
    //         arr.forEach(function(itm, idx){
    //             users.push(itm);
    //         })
    //         persistUsers(users)
    // }
    // ,   persistUsers		  = function(users){
    //         persistence.set("webness.app.users", users)
    // }
    // ,
    //     getUsers			    = function(){
    //         var users = persistence.get("webness.app.users") || [];
    //         return users;
    // }
    // ,   removeUsersById = function(id){
    //         var users = persistence.get("webness.app.users")
    //         users.forEach(function(itm, idx){
    //             if(itm.id===id){
    //                 delete users[idx];
    //             }
    //         })
    //         persistUsers(users);
    // };
        return {
            returnResults: getResults,
            find: find,
            getSearchTerm : getSearchTerm
            // persistUsers : persistUsers,
            // getUsers: getUsers,
            // removeUsersById: removeUsersById
        };
   }
);