angular.module("app.tabsService", [])
.service('tabsService', function($rootScope, persistence) {
    let config = {
        activities : {
            isVisible: true,
            list: [ 
                {
                    displayName: 'Vista Rápida',
                    id: 'overview',
                },
                {
                    displayName: 'Lugares',
                    id: 'locations',
                },
                {
                    displayName: 'Miembros',
                    id: 'members',
                },
                {
                    displayName: 'Créditos',
                    id: 'credits',
                },
                {
                    displayName: 'Pagos',
                    id: 'payments',
                },
                {
                    displayName: 'Configuración',
                    id: 'settings'
                }
            ],
            current: false
        }
    }
    ,
        setCurrentTab = function(options){
            if (!isNaN(options.index.number)){
                if(!options.listName){return false}
                let name = config[options.listName].list[options.index.number].id;
                config[options.listName].current = name;
            }
            if (options.id){
                config[options.listName].current = options.id;
            }
            // $rootScope.$broadcast('updated-tabs-config');
            let toSave = persistence.get("webness.app.tabs") || {};
            toSave[options.listName] = {};
            toSave[options.listName]['current'] = config[options.listName].current;
            persistence.set("webness.app.tabs", toSave)
            // return getCurrentTab(options.listName)
            return persistence.get("webness.app.tabs")[options.listName]
    }
    ,   getCurrentTab = function(listName){
            // return config[listName].current;
            return (persistence.get("webness.app.tabs"))?persistence.get("webness.app.tabs")[listName].current: false;
    }
    ,   handleVisibility = function(value, origin){
        if (value=='oposite'){
            config.isVisible = !config.isVisible;
        }
        else{
            config.isVisible = value;
        }
        $rootScope.$broadcast('updated-tabs-config');
    }
    ,
        getConfig = function(){
            return config;
    };
        return {
            setCurrentTab: setCurrentTab,
            handleVisibility: handleVisibility,
            getConfig: getConfig,
            getCurrentTab: getCurrentTab
        };
   }
);
