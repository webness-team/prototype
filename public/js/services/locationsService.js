angular.module("app.locationsService", [])
.service('locationsService', function($rootScope, persistence) {
    var selectedLocationId = ""
    ,
    setLocations   = function(arr){
            var locations = getLocations();
            arr.forEach(function(itm, idx){
                locations.push(itm);
            })
            persistLocations(locations);
    }
    // ,   getUsersByFriendlyUrl = function(friendlyUrl){
    //         var users = persistence.get("webness.app.users")
    //         ,   result;
    //         users.forEach(function(itm,idx){
    //             if(itm.friendlyUrl===friendlyUrl){
    //                 result = itm;
    //             }
    //         })
    //         return result;
    // }
    ,   persistLocations		  = function(locations){
            persistence.set("webness.app.locations", locations)
    }
    ,
        getLocations				= function(){
            var locations = persistence.get("webness.app.locations") || [];
            return locations;
    }
    ,   removeLocationsById = function(id){
            var locations = persistence.get("webness.app.locations")
            locations.forEach(function(itm, idx){
                if(itm.id===id){
                    delete locations[idx];
                }
            })
            persistLocations(locations);
    }
    ,
        setSelectedLocationId = function(id){
            selectedLocationId = id
        }
    ,   getSelectedLocation = function(id){
            var locations = getLocations();
            var result = {};
            locations.forEach(function(itm, idx){
                if (itm.id == selectedLocationId) {
                    result = itm;
                }
            })
            return result;
    }
    ,
    getLocationById = function(id){
        var locations = persistence.get("webness.app.locations") || [];
        var result;
        locations.forEach(function(itm, idx){
            if(itm.id===id){
                result = locations[idx];
            }
        })
        return result;
    };
        return {
            getLocationById: getLocationById,
            setLocations: setLocations,
            // getUsersByFriendlyUrl: getUsersByFriendlyUrl,
            persistLocations : persistLocations,
            getLocations: getLocations,
            removeLocationsById: removeLocationsById,
            setSelectedLocationId: setSelectedLocationId, 
            getSelectedLocation: getSelectedLocation, 
        };
   }
);