angular.module("app.subheaderService", [])
.service('subheaderService', function($rootScope, persistence) {
    let dataObj = { title : 'All your activities'}
    ,
        config = {
            height: '40px',
            isVisible : false
        }
    ,
        setData = function(data){
            // console.log('setData')
            dataObj = data;
            $rootScope.$broadcast('subheader-data-set-up');
    }
    ,   handleVisibility = function(value, origin){
        if (value=='oposite'){
            config.isVisible = !config.isVisible;
        }
        else{
            config.isVisible = value;
        }
        $rootScope.$broadcast('updated-subheader-config');
    }
    ,   setDefaultConfig = function(){
            config = {
                height: '40px',
                isVisible : false
            }
            $rootScope.$broadcast('updated-subheader-config');
    }
    ,
        getData = function(data){
            return dataObj;
    }
    ,
        getConfig = function(){
            return config;
    };
        return {
            setData : setData,
            getData : getData,
            getConfig: getConfig,
            setDefaultConfig: setDefaultConfig,
            handleVisibility: handleVisibility
        };
   }
);