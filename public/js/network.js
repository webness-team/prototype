angular.module("app.network", [])
/**
    @param options - The options that $http receives, some of the options will modified if need it
        => params       - if Falsy -> {}
        => service      - if Falsy -> false
        => data         - if Falsy -> {}
        => method       - if Falsy -> "GET"
        => headers      - if Falsy -> {}
        => cache        - if Falsy -> false
        => url          - The result of setUrlPath(opt), that will be add the "url/param/style" if extraPath exists like a 'String' or an 'String Array'
        => extraPath    - An Array or a String that will be appended to url if exist
**/
.factory('ajax', ['$http', 'serverPath', '$rootScope', '$q', 'caching', "networkConfig", "utils", function($http, serverPath, $rootScope, $q, caching, networkConfig, utils) {
    return function(options, cachingOpts) {
        var opt             = options || {}
          , baseSuccess     = function(){
                if(opt.success){
                    opt.success.apply(window, arguments);
                }
            }
          , baseFail        = function(){
                if(opt.fail){
                    opt.fail.apply(window, arguments);
                }
            }
          , setUrlPath      = function(opt){
                var response        = ""
                  , timeHash        = (new Date()).getTime();
                if(opt.extraPath){
                    if(typeof opt.extraPath === "object" && opt.extraPath.length>0){
                        response    = "/"+opt.extraPath.join("/");
                    }
                    else{
                        response    = "/"+opt.extraPath;
                    }
                };
                response    = (opt.method.toUpperCase()=="GET")?response+"?timeHash="+timeHash:response;
                return serverPath+opt.service+response;
            };

        if($rootScope.defaultHeaders){
            for(header in $rootScope.defaultHeaders){
                if(!headers.hasOwnProperty(header)){
                    opt.headers[header] = $rootScope.defaultHeaders[header]
                }
            }
        }

        opt.params          = opt.params || {};
        opt.service         = opt.service || false;
        opt.data            = opt.data || {};
        opt.method          = opt.method || "GET";
        opt.headers         = opt.headers || {};
        opt.cache           = opt.cache || false;
        opt.url             = setUrlPath(opt)

        var responseHandler = networkConfig.getDefaultHandler();

        var httpAction      = ($http(opt)
            .then(function(data){
                responseHandler(data, $rootScope)
                if(cachingOpts && data.hasOwnProperty("data")){
                    caching.save(data.data, opt.service, opt.method, cachingOpts);
                }
                return data;
            }, function(data){
                responseHandler(data, $rootScope)
                return $q.reject(data);
            }))

        return httpAction
    }
}])

.factory('server', ['ajax', '$rootScope', "caching", "networkConfig", "session", function(ajax, $rootScope, caching, networkConfig, session) {
    var     get             = function(service, opt, cachingOpts, timeout){
        var opt         = opt || {};
        opt.service     = service;
        opt.headers     = opt.headers || {};
        opt.headers.secure = session.user.getHash() || '';
        return ajax(opt, cachingOpts, timeout);
    },  post            = function(service, opt, cachingOpts, timeout){
        var opt         = opt || {};
        opt.method      = "POST";
        opt.service     = service;
        opt.headers     = opt.headers || {};
        opt.headers.secure = session.user.getHash() || '';
        return ajax(opt, cachingOpts, timeout);
    },  put             = function(service, opt, cachingOpts, timeout){
        var opt         = opt || {};
        opt.method      = "PUT";
        opt.service     = service;
        opt.headers     = opt.headers || {};
        opt.headers.secure = session.user.getHash() || '';
        return ajax(opt, cachingOpts, timeout);
    },  remove          = function(service, opt, cachingOpts, timeout){
        var opt         = opt || {};
        opt.method      = "DELETE";
        opt.service     = service;
        opt.headers     = opt.headers || {};
        opt.headers.secure = session.user.getHash() || '';
        return ajax(opt, cachingOpts, timeout);
    },  actions         = {
        "get"           : get
      , "post"          : post
      , "put"           : put
      , "remove"        : remove
    };

    var endpointList    = networkConfig.getEndpointsList();

    return new function(){
        Object.keys(endpointList).forEach(function(key, idx){
          this[key] = function(verb, options, cachingOptions){
              return endpointList[key].call(this, actions, verb, options, cachingOptions)
          }
        }, this)
    };

}])

.factory("networkStatus", ["$rootScope", "networkConfig", "utils", function($rootScope, networkConfig, utils){
    var networkStates                   = {
        "unknown"                       : "Conexión Desconocida"
      , "ethernet"                      : "Conexión Ethernet"
      , "wifi"                          : "Conexión WiFi"
      , "2g"                            : "Conexión celular 2G "
      , "3g"                            : "Conexión celular 3G "
      , "4g"                            : "Conexión celular 4G "
      , "cellular"                      : "Conexión celular generica "
      , "none"                          : "Sin Conexión"
    } , reliableConns                   = networkConfig.getReliableConnsList()
      , listenersAdded                  = false
      , getConnection                   = function(){
        var type                        = (navigator.connection)?navigator.connection.type:"unknown"
          , msg                         = networkStates[type]
          , result                      = {
                type                    : type
              , msg                     : msg
              , isReliable              : (reliableConns.indexOf(type) != -1)?true:false
            };

        return result;
    } , onOnline                        = function(e){
        $rootScope.$broadcast("onlineNetwork", e);
    } , onOffline                       = function(e){
        $rootScope.$broadcast("offlineNetwork", e);
    } , addListeners                    = function(){
        if(!listenersAdded){
            document.addEventListener("online", onOnline, false);
            document.addEventListener("offline", onOffline, false);
            listenersAdded = true;
        }
        else{
            utils.log("Listeners for App Connection already added")
        }
    };

    return new function(){
        this.addListeners   = addListeners;
        this.getConnection  = getConnection;
    };
}])

.provider('networkConfig', function() {
  var endpointsList     = {}
    , reliableConnsList = [ "wifi", "4g", "3g" ]
    , defaultHandler    = function(){
        // default does nothing
        console.log('defaultHandler')
    };
  this.setEndpoints     = function(value) {
    endpointsList       = value;
  };
  this.setReliableConns = function(value) {
    reliableConnsList   = value;
  };

  this.setDefaultHandler= function(value){
     defaultHandler     = value;
  }
  this.$get             = ["$rootScope", function($rootScope) {
    return new function(){
        this.getEndpointsList = function(){
            return endpointsList;
        };
        this.getReliableConnsList = function(){
            return reliableConnsList;
        };
        this.getDefaultHandler = function(){
            return defaultHandler
        }
    }
  }]
})
