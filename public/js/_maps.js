angular.module("app.maps", [])

.factory("maps", ["$rootScope", function($rootScope){

	var maps 				= {
		argentina : {
			options : {
			    center 			: { lat : -37.0802436 , lng : -60.0347723 }
			 ,  zoom 			: 4
			 ,  mapTypeId 		: 'roadmap' // ROADMAP - SATELLITE - HYBRID -TERRAIN
			 ,  mapTypeControl 	: false
			 , 	zoomControl 	: true
			 // , scaleControl: true
	        // panControl: true, // mapTypeControl: false, // panControlOptions: { //     position: google.maps.ControlPosition.RIGHT_CENTER // }, // zoomControl: true, // zoomControlOptions: { //     style: google.maps.ZoomControlStyle.LARGE, //     position: google.maps.ControlPosition.RIGHT_CENTER // }, // scaleControl: false, // streetViewControl: false, // streetViewControlOptions: { //     position: google.maps.ControlPosition.RIGHT_CENTER // }
			}
			,
			instance : {}
		}
    ,
    office : {
			options : {
			    center 			: { lat : -34.4745319 , lng : -58.5588879 }
			 ,  zoom 			: 12
			 ,  mapTypeId 		: 'roadmap' // ROADMAP - SATELLITE - HYBRID -TERRAIN
			 ,  mapTypeControl 	: false
			 , 	zoomControl 	: true
			 // , scaleControl: true
	        // panControl: true, // mapTypeControl: false, // panControlOptions: { //     position: google.maps.ControlPosition.RIGHT_CENTER // }, // zoomControl: true, // zoomControlOptions: { //     style: google.maps.ZoomControlStyle.LARGE, //     position: google.maps.ControlPosition.RIGHT_CENTER // }, // scaleControl: false, // streetViewControl: false, // streetViewControlOptions: { //     position: google.maps.ControlPosition.RIGHT_CENTER // }
			}
		}
	}
	, returnMapInstance 	= function(type){
		return maps[type].instance;
	}
	, newLatLng 			= function(lat, lng){
		return 	new google.maps.LatLng(lat, lng);
	}
	, create 				= function(type){
		var type			= type 			|| 'argentina'
		, 	options			= {
			    center 			: maps[type].options.center || defaultPosition
			 ,  zoom 			: maps[type].options.zoom 	|| 7
			 ,  mapTypeId 		: maps[type].options.mapTypeId || 'roadmap'
			 ,  mapTypeControl 	: maps[type].options.mapTypeControl || false
		}

		, 	latLng 			= newLatLng(options.center.lat, options.center.lng)
debugger
console.log(google)
		var a  = new google.maps.Map(document.getElementById('argentina'), {
				center: latLng,
				zoom: options.zoom,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				mapTypeControl: false
			}
		);
	}
	, center 				= function(type, latLng){
		var map 		= returnMapInstance(type);
		map.setCenter(latLng);
	}
	, setZoom 				= function(type, value){
		var map 		= returnMapInstance(type);
		map.setZoom(value);
	}
	, fitBounds 			= function(type, latLngArr){
		var latLngArr   = ensureArray(latLngArr)
		, 	map 		= returnMapInstance(type)
	    , 	bounds 		= new google.maps.LatLngBounds();

		latLngArr.forEach(function(item){
		    bounds.extend(item);
		});

	    if(bounds.getNorthEast().equals(bounds.getSouthWest())){
	       var extendPoint1 = newLatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
	       var extendPoint2 = newLatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
	       bounds.extend(extendPoint1);
	       bounds.extend(extendPoint2);
	    };

	    map.fitBounds(bounds);
	}
	, createMarkers 		= function(type, latLngArr, opt){
		var latLngArr   = ensureArray(latLngArr)
		,   opt 		= opt || {}
		,   options 	= {
			animation 	: opt.animation || false
		  , icon 		: opt.icon 		|| ''
		}
		,	map 		= returnMapInstance(type)
		,	markersArr 	= [];

		latLngArr.forEach(function(item){
		    var marker = new google.maps.Marker({
		        position 	: item,
		        map 		: map
	          , icon 		: options.icon
	          , animation 	: options.animation
		    });
			markersArr.push(marker)
		});
	}
	, addMarkers 		= function(type, latLngArr, opt){
		var opt 		= opt || {};
		createMarkers(type, latLngArr, opt);

		if(opt.fitToBounds){
			fitBounds(type, latLngArr);
		};
	}
	, getDistanceBetweenCoordinates = function(originCoors, destinationCoors){
		var R        =  6371e3;
		var φ1       =  originCoors.lat * (Math.PI / 180)
		var φ2       =  destinationCoors.lat * (Math.PI / 180)
		var Δφ       =  (destinationCoors.lat-originCoors.lat) * (Math.PI / 180)
		var Δλ       =  (destinationCoors.lng-originCoors.lng) * (Math.PI / 180)
		var a        =  Math.sin(Δφ/2) * Math.sin(Δφ/2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ/2) * Math.sin(Δλ/2);
		var c        =  2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		var d        =  R * c;
		var response = { inKm : d/1000 , inMts : d }
		return response;
	}
	, defaultPosition 			= {
		lat : 53.2734
	  , lng : -7.778320310000026
	}
	, ensureArray 				= function(toEnsure){
		if(toEnsure.constructor!=Array){
			toEnsure 	= [toEnsure]
		}
		return toEnsure;
	};

	return new function(){
		this.maps				= {
			maps				: maps
		  , returnMapInstance	: returnMapInstance
		  , newLatLng			: newLatLng
		  , create				: create
		  , center				: center
		  , setZoom				: setZoom
		  , fitBounds			: fitBounds
//	 	  , createMarkers	 	: createMarkers
		  , addMarkers	 		: addMarkers
		  , getDistanceBetweenCoordinates: getDistanceBetweenCoordinates
		}
	};

}]);
