angular.module("app.caching", [])
.factory("caching", ["persistence", "$q", function(persistence, $q){
    var data                            = {};
    var hasLocalData                    = function(serviceName, cachingOpts){
        var action                      = cachingOpts.action
        ,   actionToLowercase           = action.toLocaleLowerCase()
        ,   hasLocalData                = undefined
        if(cachingOpts.isCrossSession==true){
            hasLocalData                = (persistence.get(serviceName+"-"+actionToLowercase)!=null)?true:false;
        }
        else{
            if(data.hasOwnProperty(serviceName)){
                hasLocalData            = (data[serviceName].hasOwnProperty(actionToLowercase))?true:false;
            }
            else{
                hasLocalData            = false;
            }
        }
        return hasLocalData;
    }
    , localDataIsFresh                  = function(serviceName, cachingOpts){
        var localDataIsFresh            = undefined
        ,   maxAge                      = cachingOpts.maxAge
        ,   action                      = cachingOpts.action
        ,   actionToLowercase           = action.toLocaleLowerCase()
        ,   age                         = undefined
        ,   now                         = new Date().getTime()
        ,   timeDiff                    = undefined;
        if(cachingOpts.isCrossSession){
            var toParse                 = persistence.get(serviceName+"-"+actionToLowercase)
             ,  parsed                  = toParse
            age                         = parsed.date;
        }
        else{
            age                         = data[serviceName][actionToLowercase].date;
        }
        timeDiff                        = now - age;
        localDataIsFresh                = (timeDiff <= maxAge)?true:false;
        return localDataIsFresh;
    }
    , mustCallService                   = function(serviceName, cachingOpts){
        var isForcedCall                = cachingOpts.isForcedCall
        ,   isFresh                     = false
        ,   forcedCall                  = false
        ,   mustCallService             = false
        if(isForcedCall){
            mustCallService             = true;
        }
        else{
            if(hasLocalData(serviceName, cachingOpts)){
                mustCallService         = (localDataIsFresh(serviceName, cachingOpts))?false:true
            }
            else{
                mustCallService         = true;
            }
        }
        return mustCallService;
    }
    , recoverData                       = function(serviceName, opt, getPromise, cachingOpts){
        var response                    = undefined
        ,   source                      = undefined
        ,   cachedResponse              = undefined
        ,   action                      = cachingOpts.action
        ,   actionToLowercase           = action.toLocaleLowerCase();
        if(cachingOpts.isCrossSession){
              source                    = 'localStorage'
            , cachedResponse            = persistence.get(serviceName+"-"+actionToLowercase)
            response                    = cachedResponse.result;
        }
        else{
            source                      = 'caching module instance'
            cachedResponse              = data[serviceName][actionToLowercase];
            response                    = cachedResponse.result
        };
        var promise                     = $q(function(resolve, reject) {
            var toResolve               = { data : response};
            toResolve.data.success      = true
            toResolve.data.message      = 'response data recovered from ' + source
            resolve(toResolve);
        });
        return promise;
    }
    ,  save                             = function(responseData, serviceName, action, cachingOpts){
        var actionToLowercase           = action.toLocaleLowerCase()
          , saveAttr                    = cachingOpts.saveAttr || "success"
          , isSaveRequire               = (responseData.hasOwnProperty(saveAttr) && responseData[saveAttr])?true:false
          , now                         = new Date().getTime()
          , toSave                      = {
              date                      : now
            , maxAge                    : cachingOpts.maxAge            || 5*60000
            , isForcedCall              : cachingOpts.isForcedCall      || true
            , isCrossSession            : cachingOpts.isCrossSession    || false
            , result                    : responseData              || ''
        };
        if(cachingOpts.isCrossSession && isSaveRequire){
            persistence.set(serviceName+"-"+actionToLowercase, toSave);
        }
        else{
            if(serviceName && actionToLowercase && isSaveRequire){
                data[serviceName]                       = {}
                data[serviceName][actionToLowercase]    = toSave;
            }
        };
    }
    , clearData                 = function(){
        data = {}
    }
    return new function(){
          this.recoverData              = recoverData
        , this.mustCallService          = mustCallService
        , this.save                     = save
        , this.data                     = data
        , this.clearData                = clearData
    };

}]);
