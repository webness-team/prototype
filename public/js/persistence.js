angular.module('app.persistence', [])

.factory("persistence", [function() {
    var castToString= function(val){
        var result  = "";
        try {
            result  = (typeof val === "string")?val:JSON.stringify(val);
        }
        catch (e) {
            result  = false;
        }
        return result;
    },  set         = function(key, val){
        var strKey  = (typeof key === "string")?true:false
          , value   = castToString(val);

        if(strKey && value){
            localStorage.setItem(key, value)
        }

        return (strKey && value)?true:false;
    },  get         = function(key){
        var strKey  = (typeof key === "string")?true:false;
            result  = (strKey)?localStorage.getItem(key):null;
        try {
            var cast= JSON.parse(result);
            return cast;
        }
        catch (e) {
            return result;
        }
    },  remove      = function(key){
        var strKey  = (typeof key === "string")?true:false;
        if(strKey){
            localStorage.removeItem(key);
            return true;
        }
        return false;
    },  keys        = function(){
        var keys    = [];
        for(itm in localStorage){
            keys.push(itm);
        }
        return keys;
    };

    return new function() {
        this.set            = set;
        this.get            = get;
        this.remove         = remove;
        this.keys           = keys;
    }
}])
