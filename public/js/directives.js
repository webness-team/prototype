angular.module('app.directives', [])

.directive("keypressDetector", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {                
            document.body.onkeydown = function(e){
                if(e.keyCode === 27){
                    if(scope.overallConfig.isVisible){
                        angular.element('#overall').triggerHandler('click');
                    }
                    // https://stackoverflow.com/questions/28745553/catch-keypress-in-angularjs-service-element-independent
                }
            }
        }
    }
})

.directive("scroll", function ($window){
    return function(scope, element, attrs) {
        // console.log('scroll directive')
        angular.element($window).on("scroll", function(counter) {
            // console.log(counter)
            // console.log('scroll directive +')

            if(this.pageYOffset < 3) {
               scope.scrollOverMinimum = false;
            }
            else if(this.pageYOffset > 100){
               scope.scrollOverMinimum = true;
            };
            scope.$apply();
        });
    };
})

.directive('customBlur', function(){
    return {
      restrict: 'A',
      scope: {
        'customBlur': '='
      },
      link: function(scope, element, attr) {
        element.on('click', function(event){
          var targetAttr = angular.element(event.target).attr('custom-blur')
        //   console.log('here')
          if (typeof targetAttr !== 'undefined' && scope.customBlur) {
            scope.$apply(function(){
              scope.customBlur = false;
            });
          } 
        });
      }
}})

.directive("scrollInOverall", function ($window){
    // console.log('scrollInOverall directive')
    return function(scope, element, attrs) {
        angular.element($window).on("scroll", function(counter) {
            // console.log(counter)
            if(this.pageYOffset < 3) {
               scope.scrollOverMinimum = false;
            }
            else if(this.pageYOffset > 100){
               scope.scrollOverMinimum = true;
            };
            scope.$apply();
        });
    };
})

// .directive("calendarAction", function($window){   
//     return function(scope, element, attrs) {
//         angular.element($window).on("click", function(counter) {
//             console.log('scrollnig')
//             console.log(counter)
//         });
//     };
// })
// directive('whenScrolled', function() {
//     return function(scope, elm, attr) {
//         var raw = elm[0];
// console.log(raw)
// console.log(elm) 
//         elm.bind('scroll', function() {
//             if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
//                 scope.$apply(attr.whenScrolled);
//             }
//         });
//     };
// });


 .directive("resize", function ($window) {
     return function(scope, element, attrs) {
         angular.element($window).on("resize", function() {
            //console.log($window.innerWidth);
            scope.width= $window.innerWidth
            scope.$digest();
         });
     };
 })

.directive('gist', function() {
      return function(scope, elm, attrs) {
          var gistId = scope.gistId;

          var iframe = document.createElement('iframe');
          iframe.setAttribute('width', '100%');
          iframe.setAttribute('frameborder', '0');
          iframe.id = "gist-" + gistId;
          elm[0].appendChild(iframe);

          // var noMetadata = '.gist .gist-meta {display: none; } .gist .gist-data{border-bottom: none!important;}'
          var noMetadata = '.gist .gist-meta {visibility: collapse; } .gist .gist-meta > a:nth-child(-n+2) {visibility: visible; } '
          // var noMetadata = ''
          var style = '<style>table{font-size:12px;}' + noMetadata + '</style>'

          var iframeHtml = '<html><head><base target="_parent">'
          + style
          +'</head><body onload="parent.document.getElementById(\''
          + iframe.id
          + '\').style.height=document.body.scrollHeight + \'px\'"><script type="text/javascript" src="https://gist.github.com/'
          + gistId + '.js"></script></body></html>';

          var doc = iframe.document;
          if (iframe.contentDocument) doc = iframe.contentDocument;
          else if (iframe.contentWindow) doc = iframe.contentWindow.document;

          doc.open();
          doc.writeln(iframeHtml);
          doc.close();
      };
      // http://codersblock.com/blog/customizing-github-gists/
})

.directive("contenteditable", function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {


      function read() {
        var toPass = (element.html()=="<br>")?"":element.html()
        // ngModel.$setViewValue(element.html());
        ngModel.$setViewValue(toPass);

      }

      ngModel.$render = function() {

        var toPass = (ngModel.$viewValue=="<br>")?"":ngModel.$viewValue;
        element.html(toPass || "");


      };

      element.on("blur keyup change", function() {
        scope.$apply(read);
      });


    }
  };
})

.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.on("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });
                event.preventDefault();
            }
        });
    };
})

.directive('myBackspace', function ($window, $timeout) {
    return function (scope, element, attrs) {
        element.on("keydown keypress", function (event) {
          if(event.which === 8) {
              scope.$apply(function (){
                  // console.log('backspace');
                  scope.$eval(attrs.myBackspace);
              });
              // event.preventDefault();
          };

        });
    };
})

.directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        //scope: true,   // optionally create a child scope
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                // console.log('value=', value);
                if (value === true) {
                    $timeout(function () {
                      element[0].focus();
                      if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
                          var range = document.createRange();
                          range.selectNodeContents(element[0]);
                          range.collapse(false);
                          var sel = window.getSelection();
                          sel.removeAllRanges();
                          sel.addRange(range);
                      }
                      else if (typeof document.body.createTextRange != "undefined") {
                          var textRange = document.body.createTextRange();
                          textRange.moveToElementText(element[0]);
                          textRange.collapse(false);
                          textRange.select();
                      }
                    });
                }
            });
            // to address @blesh's comment, set attribute value to 'false'
            // on blur event:
            element.on('blur', function () {
                // console.log('blur');
                // scope.$apply(model.assign(scope, false));
                scope.$apply();
            });
        }
    };
}])

// .directive('youtubePlayer', function() {
//   return {
//     restrict: 'E',
//     scope: {
//       header: '@',
//       video: '@'
//     },
//     transclude: true,
//     replace: true,
//     template: '<iframe ng-src="{{video | trustUrl}}"></iframe>',
//     link: function(scope, element, attrs) {
//       scope.header = attrs.header;
//     }
//   };
// }