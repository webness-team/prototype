angular.module("app.calendar", [])
// .service("calendar", ["persistence", "$q", function( persistence, $q, $rootScope){
.service('calendar', function($rootScope, persistence) {

    var languageData           = {
        days                    : ["Dom","Lun","Mar","Mie","Jue","Vie","Sab"],
        daysFullText            : ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"],
        months                  : ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
        monthsFullText          : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
    }
    ,
    options = {
        // sixWeeksPerMonth        : options.sixWeeksPerMonth || false,
        // weekStartDay            : (options.weekStartDay && options.weekStartDay < 7)?options.weekStartDay:0,
        // actualDate              : options.actualDate || new Date() 
        sixWeeksPerMonth        :  false,
        weekStartDay            : 0,
        actualDate              : new Date() 
    }
    ,
    getWeeksArr = function(){
        var date                    = new Date(options.actualDate.getTime()),
            monthWeeks              = [],
            self                    = this;
    
        date.setDate(1);
    
        for(var i = 1; i<=6; i++){
            monthWeeks.push(getDaysArr(date));
            date.setDate(date.getDate()+7);
        }
    
        if(!options.sixWeeksPerMonth && (monthWeeks[1][0].monthNumber != monthWeeks[5][0].monthNumber)){
            monthWeeks.pop();
        }
    
        return monthWeeks;
    }
    ,
    getDaysArr = function(date){
        var weekDate                = (date && (date.constructor == new Date().constructor))?new Date(date.getTime()):new Date(options.actualDate.getTime()),
            daysToWeekStart         = utils().daysToWeekStart(weekDate),
            weekArr                 = [];
        
        weekDate.setHours(24*daysToWeekStart)
        for(var i = 0; i<7; i++){
            weekArr[i]              = utils().buildDayObj(weekDate);
            weekDate.setHours(+24);
        }
        return weekArr;
    }
    ,
    utils = function(){

        var self                    = this,
            daysToWeekStart         = function(weekDate){
                var diff            = options.weekStartDay - weekDate.getDay(),
                    result          = (options.weekStartDay > weekDate.getDay())?(diff-7):diff;
                return result;
            }, 
            buildDayObj             = function(date){
                return {
                    dayText         : languageData.daysFullText[date.getDay()],
                    dayNumber       : date.getDate(),
                    monthText       : languageData.monthsFullText[date.getMonth()],
                    monthNumber     : date.getMonth(),
                    year            : date.getFullYear()
                };
            }, 
            updateDate              = function(difference, type){
                var difference      = difference || 0,
                    isInt           = difference % 1 === 0,
                    types           = {
                        day         : {set:"setDate",       get:"getDate",      multiplier:1},
                        week        : {set:"setDate",       get:"getDate",      multiplier:7},
                        month       : {set:"setMonth",      get:"getMonth",     multiplier:1},
                        year        : {set:"setFullYear",   get:"getFullYear",  multiplier:1}
                    },
                    type            = types[type] || false;
            
                if(difference!=0 && isInt && type){
                    options.actualDate[type.set](options.actualDate[type.get]()+(difference*type.multiplier));
                }
            };
    
        return {
            daysToWeekStart         : daysToWeekStart,
            buildDayObj             : buildDayObj,
            updateDate              : updateDate
        };
    }
    ,
    setOtherDay = function(difference){
        utils().updateDate(difference, "day");
    }
    ,
    setOtherWeek = function(difference){
        utils().updateDate(difference, "week");
    }
    ,
    setOtherMonth  = function(difference){
        utils().updateDate(difference, "month");
    }
    ,
    setOtherYear  = function(difference){
        utils().updateDate(difference, "year");
    }
    // custom
    ,
    selectedDay = {}
    ,
    currentWeeksArr = []
    ,
    previuosRoute = '/'
    ,
    setCurrentWeeksArr = function(arr){
        currentWeeksArr = arr
    }
    ,
    getCurrentWeeksArr = function(){
        return currentWeeksArr;
    }
    ,
    addWeeksToCurrentArr = function(weeksArr, deleteDuplicates, direction){
        var isPast = (direction=='left')?true:false;
        let weeksArrDuplicate = Object.assign([], weeksArr);
        // console.log(weeksArrDuplicate)
        let result = Object.assign([], currentWeeksArr);
        if(deleteDuplicates){
            if(isPast){
                weeksArrDuplicate.pop();

                // borrar las ultimas 5 semanas
                result.pop();
                result.pop();
                result.pop();
                result.pop();
                result.pop();
            }
            else{
                weeksArrDuplicate.splice(0,1);
                // borrar las primeras 5 semanas
                result.splice(0,1);
                result.splice(0,1);
                result.splice(0,1);
                result.splice(0,1);
                result.splice(0,1);
            }
        }
        weeksArrDuplicate.forEach(function(itm, idx){
            if(isPast){
                result.splice(idx, 0, itm); // result.unshift(itm);
            }
            else{
                result.push(itm);
            }
        })
        setCurrentWeeksArr(result);
    }
    ,
    setPreviuosRoute = function(route){
        previuosRoute = route
    }
    ,
    getPreviuosRoute = function(route){
        return previuosRoute;
    }
    , 
    setSelectedDay = function(day){
        selectedDay = day;
        $rootScope.$broadcast('updated-calendar-selected-day');
    }
    , 
    getSelectedDay = function(day){
        return selectedDay;
    };

    return new function(){
          this.getWeeksArr      = getWeeksArr;
          this.getDaysArr       = getDaysArr;
          this.utils            = utils();
          this.setOtherDay      = setOtherDay;
          this.setOtherWeek     = setOtherWeek;
          this.setOtherMonth    = setOtherMonth;
          this.setOtherYear     = setOtherYear;
    // custom
          this.setSelectedDay = setSelectedDay;
          this.getSelectedDay = getSelectedDay;

          this.setPreviuosRoute = setPreviuosRoute;
          this.getPreviuosRoute = getPreviuosRoute;
          this.setCurrentWeeksArr  = setCurrentWeeksArr;
          this.getCurrentWeeksArr  = getCurrentWeeksArr;
          this.addWeeksToCurrentArr  = addWeeksToCurrentArr;
    };

});

