angular.module("app.utils", [])

// .factory("utils", ["maps", "$rootScope", "session", "$timeout", function($rootScope, session, $timeout){
.factory("utils", ["$rootScope", "session", "$timeout", function($rootScope, session, $timeout){

	// var map 					= {
	// 	create 	: function(type){
	// 		maps.maps.create(type)
	// 	}
	//   , newLatLng	: function(lat, lng){
	//   		maps.maps.newLatLng(lat, lng)
	//   }
	//   , center	: function(type, latLng){
	//   		maps.maps.center(type, latLng)
	//   }
	//   , setZoom	: function(type, value){
	//   		maps.maps.setZoom(type, value)
	//   }
	//   , getDistanceBetweenCoordinates: function(origin, destination){
	//   		maps.maps.getDistanceBetweenCoordinates(origin, destination)
	//   }
	//   , fitBounds : function(type, bounds){
	//   		maps.maps.fitBounds(type, bounds)
	//   }
	//   , addMarkers : function(type, latLngArr, opt){
	//   		maps.maps.addMarkers(type, latLngArr, opt)
	//   }
	// };

	var global = {
    	message : ''
    	, show : false
		, isClosable: false
		, type: '' // success, info, warning, danger
	};
	var info 							= {
			get   : function(){
				return global;
			}
		,	update 		: function(msg){
				global.message = msg;
			}
		, displayOff : function(){
			global.show = false;
			$rootScope.$broadcast('message-updated');
		}
		, displayOn : function(options){
			global.show = true;
			global.isClosable = true;
			global.type = options.type;
			$rootScope.$broadcast('message-updated');
		}
		, alert : {
				show : function(message, opt){
					var options = { time : opt.time || 25000, isClosable : opt.isClosable || true , type : opt.type || 'success' };
					info.update(message);
					info.displayOn(options);
		          $timeout(info.displayOff, options.time);
			}
			, close : function(){
				var message = '';
				info.update(message);
				info.displayOff();
			}
		}
	};

	var clientSession 					= {
			open : function(user){
				session.user.set(user);
		}
		,
			close : function(){
				session.close();
		}
		, getUser : function(){
				return session.user.get();
		}
		, getHash : function(){
				return session.user.getHash();
		}
	};

	var util 					= {
		   getObjVal : function (obj, path, separator){
			var result						= undefined
			  , separator					= separator || "."
			  , path						= path || ""
			  , pathArr						= (path.constructor === Array)?path:path.split(separator)
			  , reduceFn					= function(prev, act){
				    return (prev)?prev[act]:undefined;
				};
			return pathArr.reduce(reduceFn, obj);
		}
		,  getType : function (obj){
			var result						= false;
			if([null, undefined].indexOf(obj)==-1){
				result						= obj.constructor.toString().toLowerCase().match(/\s([a-z]*)/)[1];
			}else{
				result						= ""+obj;
			}
			return result;
		}
		,  base64Decode : function (param, isJson){
			var base64						= new Buffer(param, 'base64').toString()
			  , result						= (isJson)?(JSON.parse(base64)):base64;
			return result;
		}
		,  tryCatchWrapper : function (toExecute, onError){
			try{
				toExecute();
			}
			catch(e){
				onError();
			}
		}
		,  ensureArray : function(toEnsure){
			if(toEnsure.constructor!=Array){
				toEnsure 		= [toEnsure]
			}
			return toEnsure;
		}
		,  setTimeout : function(func, delay){
			setTimeout(func, delay);
		}
	 	,  toTitleCase : function(string){
			return string.replace(/\w\S*/g, function(txt){
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			});
		}
	};

	var reloadRoute 		= function(){
		$route.reload();
	}

	return new function(){
		// this.map 			= {
		// 	// getInstance : map.returnMapInstance,
		//     create 	: map.create
		//   , newLatLng : map.newLatLng
		//   , center	: map.center
		//   , setZoom	: map.setZoom
		//   , getDistanceBetweenCoordinates: map.getDistanceBetweenCoordinates
		//   , fitBounds	: map.fitBounds
		//   , addMarkers : map.addMarkers
		// };

		this.info 			= {
				get : info.get
			, update : info.update
			, displayOff : info.displayOff
			, displayOn : info.displayOn
			, alert : info.alert
		};

		this.session 	= {
					open : clientSession.open
				, close : clientSession.close
				, getUser : clientSession.getUser
				, getHash : clientSession.getHash
		};

		this.util 			= {
			  getObjVal			: util.getObjVal
			, getType			: util.getType
			, base64Decode		: util.base64Decode
			, tryCatchWrapper	: util.tryCatchWrapper
			, ensureArray		: util.ensureArray
			, setTimeout 		: util.setTimeout
			, toTitleCase 		: util.toTitleCase
			, reloadRoute 	: reloadRoute
		};

	};

}]);
