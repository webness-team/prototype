var app = angular.module('app', [
  'ngRoute',
  'app.network',
  'app.session',
  // 'app.activities',
  'app.utils',
  'app.constants',
  'app.persistence',
  'app.caching',
  'app.directives',

  'app.activitiesService',
  'app.meetingsService',
  'app.locationsService',
  'app.usersService',

  'app.subheaderService',
  
  'app.calendar',
  'app.searchService',
  'app.tabsService',

  'app.gui.overallService',
  // , 'ngProgress'
])

.run(['$rootScope','$location', '$routeParams', '$window', function($rootScope, $location, $routeParams, $window) {
    $rootScope.currentPath = '/';
    // var routesThatDontRequireAuth = [
    //     '/',
    //     '/signin', '/signup', '/lost', '/forgot', '/reset'
    //   , '/error/page-not-found'
    // ];
    var routesThatRequireAuth = [
        '/activities', 
        '/account',
        '/calendar'
    ];

    var routesThatRequireSubheader = [
      '/activities', 
      '/calendar'
  ];

    // $rootScope.$on('$locationChangeStart', function(event, next, current) {
    //   var output = next.replace("http://localhost:5000/", "");
    //   var isAllowed           = (notAllowedArr.indexOf(output) != -1)?false:true;
    //   if(!isAllowed && !$rootScope.activeSession){
    //      event.preventDefault();
    //      $rootScope.$broadcast('session-lost');
    //   }
    //   // var currentRouteRequiresAuth = (routesThatDontRequireAuth.indexOf($location.url()) == -1)?false:true;
    //   // if (currentRouteRequiresAuth && !$rootScope.activeSession) {
    //   //    event.preventDefault();
    //   //    $rootScope.$broadcast('session-lost');
    //   // }
    // });

    $rootScope.$on('$routeChangeStart', function(event, current, pre) {
      $rootScope.$broadcast('update-content-bg', { bg : { color: '#fff' }});

      // var currentRouteRequiresAuth = (routesThatDontRequireAuth.indexOf($location.url()) != -1)?false:true;
      var locationToVerify = '/'+$location.url().split('/')[1];
      var currentRouteRequiresAuth = (routesThatRequireAuth.indexOf(locationToVerify) == -1)?false:true;

      var currentRouteRequiresSubheader = (routesThatRequireSubheader.indexOf(locationToVerify) == -1)?false:true;

  
      $rootScope.$broadcast('handling-subheader-visibility', { data: { value : currentRouteRequiresSubheader } });
      if (currentRouteRequiresAuth && !$rootScope.activeSession) {
         event.preventDefault();
         $rootScope.$broadcast('session-lost');
      }
    });

    $rootScope.$on('$routeChangeSuccess', function(event, current, pre) {
      $rootScope.currentPath = $location.path();
    });

    window.onbeforeunload = function () {
       console.log('onbeforeunload')
    };
    
}]);

app.config(['$routeProvider', '$locationProvider', 'networkConfigProvider', 'endpoints', 'responseHandler', function($routeProvider, $locationProvider, networkConfigProvider, endpoints, responseHandler){
  networkConfigProvider.setEndpoints(endpoints)
  networkConfigProvider.setDefaultHandler(responseHandler)
  networkConfigProvider.setReliableConns(["ethernet", "wifi", "2g", "3g", "4g", "cellular", "unknown"])

  $locationProvider.hashPrefix('');
  $locationProvider.html5Mode(true);
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

  $routeProvider

  .when('/', {
    templateUrl : 'public/modules/home/home.html',
    controller  : 'HomeCtrl'
  })

  .when('/dashboard', {
    templateUrl : 'public/modules/home/home.html',
    controller  : 'HomeCtrl'
  })

  
  .when('/activities', {
    templateUrl : 'public/modules/activities/activities.html',
    controller  : 'ActivitiesCtrl',
    cache: true
  })
  .when('/activities/:friendlyUrl', {
    templateUrl : 'public/modules/activities/activities/activity.html',
    controller  : 'ActivityCtrl',
    cache: false
  })

  .when('/activities/:friendlyUrl/tabs/:tab', {
    templateUrl : 'public/modules/activities/activities/activity.html',
    controller  : 'ActivityCtrl',
    cache: false
  })


  .when('/activities/:friendlyUrl/calendar/:year?/:month?/:day?', {
    templateUrl : 'public/modules/calendar/calendar.html',
    controller  : 'CalendarCtrl',
    // cache: true
  })

  .when('/calendar/:year?/:month?/:day?', {
    templateUrl : 'public/modules/calendar/calendar.html',
    controller  : 'CalendarCtrl'
  })

  .when('/forgot', {
    templateUrl : 'public/modules/auth/forgot/forgot.html',
    controller  : 'ForgotPasswordCtrl'
  })

  .when('/reset/:resetPasswordToken?', {
    templateUrl : 'public/modules/auth/reset/reset.html',
    controller  : 'ResetPasswordCtrl'
  })

  .when('/signup', {
    templateUrl : 'public/modules/auth/signup/signup.html',
    controller  : 'SignUpCtrl'
  })

  .when('/signin', {
    templateUrl : 'public/modules/auth/signin/signin.html',
    controller  : 'SignInCtrl'
  })
  .when('/account', {
    templateUrl : 'public/modules/account/account.html',
    controller  : 'AccountCtrl'
  })
  .when('/user/:username', {
    templateUrl : 'public/modules/account/account.html',
    controller  : 'AccountCtrl'
  })


  .when('/error/page-not-found', {
    templateUrl : 'public/modules/404/404.html',
    controller  : '404Ctrl'
  })

  .otherwise({redirectTo:'/error/page-not-found'});

}])
