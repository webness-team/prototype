module.exports						= base;

function base(moduleName){
	this.moduleName					= moduleName;
	this.restUtils					= new (require("../rest/restUtils"))();
	this.network						= require("api-connection");
	this.interpreter        		= require("../interpreter/interpreter");
	this.joi 								= require("joi");
	this.nodemailer 				= require("nodemailer");
}

// base.prototype.test					= function(interpreter, data, isRequest){
// 	var interpreter					= interpreter || {}
// 	  // , method						= (isRequest)?"client2server":"server2client";
// 	return interpreter.test();
// }
