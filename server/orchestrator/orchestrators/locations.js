module.exports                      = locations;
var base                                = require("./../base")
  // , userstore                          = new (require("./../../persistence/business/userStore"))();
function locations(){
  this.store = require('./../../store/db').instance;
  this.helper = new(require("./../../commons/helper"))();
};
locations.prototype                     = new base("locations");
locations.prototype.init = function(data, req, callback){
  this.request = req;
  return this[req.entryMethod](data, req, callback);
};

locations.prototype.getLocations    = function(data, req, callback){
    let self        = this
    ,   res         = self.restUtils.buildJSONResponse('fail')
    ,   toResponse  = {}
    ,   sort        = {} // data.sort       || {}
    ,   limit       = data.limit || false 
    ,   idStringArray     = req.query.id.split(',');
    let query = {};

    if (idStringArray[0]=='all') {
        query           = {};
    } else {
        let idObjectArray = [];
        idStringArray.forEach(function(itm, idx){
            let objId;
            try {
              objId     = self.store.ObjectId(itm);
            }
            catch(e){
              callback(res)
            }
            idObjectArray.push(objId);
        })
    
        query       = { '_id': { $in: idObjectArray } };
    }

    self.store.get({collection: 'locations', query : query, limit : limit , sort : sort}, function(error, dbData){
        if(error){
            // do nothing
        }
        else{
            var hasMatchs           = (dbData.length>0)?true:false;
            if(hasMatchs){
                var result = [];
                dbData.forEach(function(itm, idx){
                  result.push({
                    "id": dbData[idx]['_id'],
                    "title": dbData[idx]['title'],
                    "address": dbData[idx]['address'],
                    "coors": dbData[idx]['coors'],
                    "imageUrl": dbData[idx]['imageUrl']
                  })
                })
                toResponse          = { data : { items : result, itemsLength: dbData.length} }
                var  interpreter    = new self.interpreter(self.moduleName+"/"+"locations");
                res                       = interpreter.JSON2Response(toResponse)
                res.response.message    = "succesfully returned locations";
            }
            else{
                res.response.error      = { error : { code : "200" , title : "no locations available", detail : "no locations available" } };
                res.response.message    = "no locations available";
            }
        };
        callback(res);
    });
};
locations.prototype.postLocations    = function(data, req, callback){
    let self         = this
    ,   res          = self.restUtils.buildJSONResponse('fail');
    
    var dataToRecord = {
        type: data.type || '' ,
        title: data.title,
        address: data.address,
        imageUrl: data.imageUrl || '',
        coordinates: data.coordinates || { lat: '', lng: ''},
        is_active: true
    };
    const locationSchema = self.joi.object().keys({
        type: self.joi.string().allow(''), 
        title: self.joi.string().allow(''), 
        address: self.joi.string().allow(''), 
        imageUrl: self.joi.string().allow(''), 
        coordinates: self.joi.object().keys({
                lat: self.joi.string().allow(''),
                lng: self.joi.string().allow(''),
            }), 
        is_active: self.joi.boolean().required(),
    });
    try {
        self.joi.assert(dataToRecord, locationSchema);
    } catch (e) {
        res.response.error    = { error : { code : "200" , title : "invalid parameter" }};
        res.response.message  = "title" + " " + "invalid parameter";
        return callback(res);
    }
       
    self.store.post({ collection : 'locations', data : dataToRecord }, 
    function(error, postResponse){
        if(error){
        res.response.error    = { error : { code : "200" , title :  error.message }};
        res.response.message  = error.message;
        callback(res);
        }
        else{
            if(data.activity_id){
            // recently_created_location_id ==> postResponse.ops[0]._id.toString();
                let updateRelatedMeetings = function(data, req, callback){
                    if(data.meeting_id){
                        data.toUpdate = {
                            collectionName: 'meetings', 
                            registryId: data.meeting_id, 
                            data: {
                                id: postResponse.ops[0]._id.toString(), 
                                fieldName: 'locations', 
                            }                            
                        };
                        self.helper.updateRelationsInDb(data, req, callback);
                    }
                    else{
                        callback({status: 200, response: { 
                            message: 'location was created',
                            success: true,
                            data: {
                            location_id: postResponse.ops[0]._id.toString()
                            }
                        }});  
                    };
                };
                data.toUpdate = {
                    collectionName: 'activities', 
                    registryId: data.activity_id, 
                    data: {
                        id: postResponse.ops[0]._id.toString(), 
                        fieldName: 'locations', 
                    }                            
                };
                self.helper.updateRelationsInDb(data, req, callback, updateRelatedMeetings);
            }else{
                callback({status: 200, response: { 
                    message: 'location was created',
                    success: true,
                    data: {
                    location_id: postResponse.ops[0]._id.toString()
                    }
                }});      
            };
        };
    });
};