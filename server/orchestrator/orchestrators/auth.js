module.exports                      = auth;
var commons                         = require('./../../commons/commons')
var base                                = require("./../base");

function auth(){
  this.store = require('./../../store/db').instance;
  this.userStore = new(require('./../../store/userStore'))();
  this.secure = new (require('api-secure'))(commons.constants.security);
  this.apiMail = new(require("./../../commons/api-mail"))({config: commons.constants.apiMail});
  this.helper = new(require("./../../commons/helper"))();
  // this.headers                   = new (require("./headers"))("auth");
};
auth.prototype = new base("auth");

auth.prototype.init = function(data, req, callback){
  this.request = req;
  return this[req.entryMethod](data, req, callback);
};

auth.prototype.signIn = function(data, req, callback){
  this.request                    = req;
  var self 				= this
  ,   res         = self.restUtils.buildJSONResponse('fail')
  ,   toResponse  = {};
  const credentials = {
    email: data.email || '',
    username: data.username || '',
    password: data.password
  };
  const uniqueIdentifier = (credentials.email.length>0)?'email':'username';

  var onGet = function(error, dbData){
    if(error){
        res.response.error 		= { error : { code : "200" , title : "communication with data base failed", detail : "data base server responded with an error" } };
        res.response.message 	= "unable to connect to data base";
    }else{
        var hasMatchs           = (dbData.length>0)?true:false;
        if(hasMatchs){
            var firstDocMatch   = dbData[0];
            var hasPasswordMatch= (firstDocMatch['password']==credentials['password'])?true:false;
            if(hasPasswordMatch){
                var userInfo = {
                    params: {
                      email: dbData[0].email
                    , user_type         : dbData[0].type
                    , _id				: dbData[0]._id.toString()
                    , tokens			: [tokenObj]
                    , lastLogin			: new Date().getTime()
                    // , appVersion		: self.request.get("appVersion") || ''
                    }
                  };
                  var tokenObj 		= self.secure.create(userInfo, true);
                  var query = {};
                  query[uniqueIdentifier] = credentials[uniqueIdentifier];
                  self.store.put({ 
                    collection : 'users', 
                    query : query, 
                    update: { 
                          tokens				: [tokenObj]
                        , lastLogin			: new Date().getTime()
                     }  }, 
                    callback({status:200, response: 
                    { data : {
                            user_type: firstDocMatch.type,
                            email :firstDocMatch.email,
                            username : firstDocMatch.username,
                            name : firstDocMatch.name,
                            surname : firstDocMatch.surname,
                            role : firstDocMatch.role,
                            token: tokenObj.token,
                            profile_pic: firstDocMatch.profile_pic,
                            id: firstDocMatch['_id']
                        },
                        success: true
                      }
                    })
                )
            }else{
                res.response.error 		= { error : { code : "200" , title : "authentication failed", detail : "password incorrect" } };
                res.response.message 	= "the password was incorrect";
                callback(res);
            }
        }else{
            res.response.error 		= { error : { code : "200" , title : "authentication failed", detail : "username or email was incorrect" } };
            res.response.message 	= "username or email was incorrect";
            callback(res);
        }
    }
  }

  if(credentials.email && credentials.email.length > 0 ){
    self.userStore.getByEmail(credentials.email, onGet);
  }
  else{
    self.store.get({ collection : 'users', query : { username : credentials.username } }, onGet) 
  };
  
};
auth.prototype.sendPasswordRecoveryEmail = function(data, req, callback){
  let self         = this
  ,   res          = self.restUtils.buildJSONResponse('fail');

  var onGet = function(error, dbData){
    if(error){
      res.response.error 		= { error : { code : "200" , title : "communication with data base failed", detail : "data base server responded with an error" } };
      res.response.message 	= "unable to connect to data base";
    }else{
      var hasMatchs           = (dbData.length>0)?true:false;
      if(hasMatchs){
          var firstDocMatch   = dbData[0];
          var passwordToken 		= 
          self.secure.create(  {
            params: Object.assign({}, {
              email: data.email ,
            })
          }, true);  

          self.store.put({ 
            collection : 'users', 
            query : { email: firstDocMatch.email }, 
            update: { passwordToken: passwordToken }  }, 
            function(error, dbData){
              if(error){
                res.response.error    = { error : { code : "200" , title :  error.message }};
                res.response.message  = error.message;
                callback(res);
              }
              else{

                var toPass = {
                  from: '"Webness Staff" <noreply.webness@gmail.com>',
                  to: firstDocMatch.email,
                  subject: "Forgotten Password Reset", 
                  template: 'password_forgot',
                  context: { name: firstDocMatch.name, 
                             link: 'http://localhost:6006/reset/' + passwordToken.token
                            }
                };
              
                self.apiMail.sendMail(toPass, function(error, info){
                    if(error){
                      callback({status: 409, response: {success: true, message: 'error: password recovery email was not sent'}})
                    }else{
                      callback({status: 200, response: {success: true, message: 'password recovery email was sent'}})
                    }
                });

              }
            })
      }
      else{
        res.response.error 		= { error : { code : "200" , title : "authentication failed", detail : "username or email was incorrect" } };
        res.response.message 	= "username or email was incorrect";
        callback(res);
      }
    }
  }

  self.userStore.getByEmail(data.email, onGet);

};
auth.prototype.updatePassword = function(data, req, callback){
  this.request                    = req;
  var self 				= this
  ,   res         = self.restUtils.buildJSONResponse('fail')
  const newPassword = data.password;
    // check token is valid 
  const tokenToValidate = data.token;

    var onGet = function(error, dbData){
      if(error){
        //toDo: handle error
      }
      else{
        var hasMatchs           = (dbData.length>0)?true:false;
        if(hasMatchs){
            var firstDocMatch   = dbData[0];
            if(new Date(dbData[0].passwordToken.expire) > new Date()){
                  self.store.put({ 
                    collection : 'users', 
                    query : { email: dbData[0].email }, 
                    update: { password: newPassword, passwordToken: {} }  }, 
                    function(error, dbPutResponse){
                      if(error){
                        res.response.error    = { error : { code : "200" , title :  error.message }};
                        res.response.message  = error.message;
                        callback(res);
                      }
                      else{
                        callback({status:200, response: {
                          success: true,
                          data: dbPutResponse
                        }})
                      }
                    })
            }
            else{
              res.response.message  = "token was expired";
              return callback(res);
            }
        }
        else{
          // habdle error
          res.response.message  = "user not found";
          return callback(res);
        }
      }
    }
    var query = { 'passwordToken.token': tokenToValidate };
    this.store.get({ collection : 'users', query : query }, onGet) 
    // toDo: validate token 
    // toDo: validate password
};

auth.prototype.getUsers = function(data, req, callback){
  let self         = this
  ,   res          = self.restUtils.buildJSONResponse('fail');
  
  let idStringArray = req.query.id.split(',');
  let idObjectArray = [];
  let query         = {};

  if (idStringArray[0]=='all') {
    query           = {};
  } else {
    idStringArray.forEach(function(itm, idx){
      let objId;
      try {
        objId     = self.store.ObjectId(itm);
      }
      catch(e){
        callback(res)
      }
      idObjectArray.push(objId);
    })  
    query          = { '_id': { $in: idObjectArray } };
  }

  // admin token validation
  // if(req.tokenInfo.user_type!='admin'){
  //   return callback(res);
  // }

  this.store.get({ collection : 'users', query : query }, 
    function(error, dbData){
      if(error){
        res.response.error    = { error : { code : "200" , title :  error.message }};
        res.response.message  = error.message;
        callback(res);
      }
      else{
        var result = [];
        dbData.forEach(function(itm, idx){
          result.push({
            "id": dbData[idx]['_id'],
            "type": dbData[idx]['type'],
            "name": dbData[idx]['name'],
            "surname": dbData[idx]['surname'],
            "email": dbData[idx]['email'],
            "username": dbData[idx]['username'],
            "born_date": dbData[idx]['born_date'],
            "account_is_confirmed": dbData[idx]['account_is_confirmed'],
            "lastLogin": dbData[idx]['lastLogin']
          })
        })
        callback({status:200, response: {
          success: true,
          data : { items : result, itemsLength: result.length} 
        }})
      }
   })
};
auth.prototype.createUsers = function(data, req, callback){
  let self         = this
  ,   res          = self.restUtils.buildJSONResponse('fail');

    this.store.get({ collection : 'users', query : { email: data.email } }, 
      function(error, dbData){
        if(error){
          res.response.error    = { error : { code : "200" , title : "communication with data base failed", detail : "data base server responded with an error" } };
          res.response.message  = "unable to connect to database";
        }
        else{
          if(dbData.length>0){
            res.response.message  = "email already exists"; 
            callback(res)        
          }
          else{

          var dataToRecord = {
                type: data.type || '' ,
                name: data.name || '' ,
                surname: data.surname || '' ,
                email: data.email || '' ,
                password: data.password || '' ,
                username: (data.username!='')?data.username:data.email.slice(0, data.email.indexOf('@')),
                born_date: data.born_date || '',
                is_active: true,
                account_is_confirmed: false,
                tokens: [],
                passwordToken: {},
                profile_pic: data.profile_pic || "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Microsoft_Account.svg/200px-Microsoft_Account.svg.png"
          };
          const userSchema = self.joi.object().keys({
            type: self.joi.string().required(),
            name: self.joi.string().required(),
            surname: self.joi.string().required(),
            email: self.joi.string().required(),
            password: self.joi.string().required(),

            username: self.joi.string().allow(''),
            born_date: self.joi.string().allow(''),
            is_active: self.joi.boolean().required(),
            account_is_confirmed: self.joi.boolean().required(),
            tokens: self.joi.array(), //.allow(''),
            passwordToken: self.joi.object(), //.keys({}), 
            profile_pic: self.joi.string().allow(''),
        });
        try {
            self.joi.assert(dataToRecord, userSchema);
        } catch (e) {
            res.response.error    = { error : { code : "200" , title : "invalid parameter" }};
            res.response.message  = "title" + " " + "invalid parameter";
            return callback(res);
        }

          self.store.post({ collection : 'users', data : dataToRecord }, 
            function(error, postResponse){
              if(error){
                res.response.error    = { error : { code : "200" , title :  error.message }};
                res.response.message  = error.message;
                callback(res);
              }
              else{

                if(data.activity_id){
                  // recently_created_user_id ==> postResponse.ops[0]._id.toString();
                  let updateRelatedMeetings = function(data, req, callback){
                      if(data.meeting_id){
                          data.toUpdate = {
                            collectionName: 'meetings', 
                            registryId: data.meeting_id, 
                            data: {
                                id: postResponse.ops[0]._id.toString(), 
                                fieldName: 'members', 
                            }                            
                        };
                        self.helper.updateRelationsInDb(data, req, callback, updateRelatedMeetings);
                      }
                      else{
                          callback({status: 200, response: { 
                              message: 'user was created',
                              success: true,
                              data: {
                              location_id: postResponse.ops[0]._id.toString()
                              }
                          }});  
                      }
                  }
                  data.toUpdate = {
                    collectionName: 'activities', 
                    registryId: data.activity_id, 
                    data: {
                        id: postResponse.ops[0]._id.toString(), 
                        fieldName: 'members', 
                    }                            
                  };
                  self.helper.updateRelationsInDb(data, req, callback, updateRelatedMeetings);
                }else{
                    callback({status: 200, response: { 
                        message: 'user was created',
                        success: true,
                        data: {
                        user_id: postResponse.ops[0]._id.toString()
                        }
                    }});      
                };
              };
          })
        }
      }
    });
};
// needs special token (type 'admin', or one that satisfies this condition ===> user.user_type === token.user_type)
auth.prototype.updateUsers = function(data, req, callback){
    let self         = this
    ,   res          = self.restUtils.buildJSONResponse('fail');
    // toDo: see how to make this validation: 
    // USER to perform UPDATE action ==> needs its TOKEN-user_type equal to target's USER-user_type or 'ADMIN'
    this.store.put({ 
      collection : 'users', 
      query : { email: data.email }, 
      update: data  }, 
      function(error, dbData){
        if(error){
          res.response.error    = { error : { code : "200" , title :  error.message }};
          res.response.message  = error.message;
          callback(res);
        }
        else{
          callback({status:200, response: {
            success: true,
            data: dbData,
            message: 'usuario actualizado'
          }})
        }
      })
};
auth.prototype.disableUsers = function(data, req, callback){
  let self         = this
  ,   res          = self.restUtils.buildJSONResponse('fail');

    // admin token validation
    // if(req.tokenInfo.user_type!='admin'){
    //   return callback(res);
    // }

    this.store.put({ 
      collection : 'users', 
      query : { email: data.email }, 
      update: { is_active: false }  }, 
      function(error, dbData){
        if(error){
          res.response.error    = { error : { code : "200" , title :  error.message }};
          res.response.message  = error.message;
          callback(res);
        }
        else{
          callback({status:200, response: {
            success: true,
            data: dbData
          }})
        }
      })
    // needs TOKEN type equal to USER type or ADMIN
};
//toDo
// auth.prototype.enableUsers = function(data, req, callback){}

auth.prototype.inviteNewMembers = function(data, req, callback){
  let self         = this
  ,   res          = self.restUtils.buildJSONResponse('fail');
  self.callback    = callback;
  // CHECK IF EMAIL EXISTS IN DATABASE
    this.store.get({ collection : 'users', query : { email: data.email } }, 
      function(error, dbData){
        if(error){
          res.response.error    = { error : { code : "200" , title : "communication with data base failed", detail : "data base server responded with an error" } };
          res.response.message  = "unable to connect to database";
        }
        else{
          const passwordToken = { token: "[PASSWORD_TOKEN]"};          
          var toPass = {
            from: '"Webness Staff" <noreply.webness@gmail.com>',
            to: data.email,
            subject: "Invitation To Join", 
            template: 'invitation_to_join_confirm_account',
            context: {  name: data.name, 
              link: 'http://localhost:6006/account/confirm/' + passwordToken.token, // activityName: '', // meetingName: '', // meetingDetails: {}, // inviter: '',
            }
          };
          if(dbData.length>0){
            // A - USER/EMAIL ALREADY EXISTS: 
              // 1. SEND INVITATION NOTIFICATION EMAIL              
              // 2. ADD USER TO ACTIVITY, ADD USER TO MEETING (IF IDS PROVIDED)
            toPass.context = {  name: data.name, 
              link: 'http://localhost:6006/activities/' + data.activity_friendly_url,
            }
            toPass.template = 'invitation_to_join_notification';
            // SEND INVITATION NOTIFICATION EMAIL
            self.apiMail.sendMail(toPass, function(error, info, callback){
                if(error){
                  callback({status: 409, response: {success: true, message: 'error: invitation was not sent'}})
                } else {                      
                //
                  // ADD USER TO ACTIVITY, ADD USER TO MEETING
                  if(data.activity_id){
                    let updateRelatedMeetings = function(data, req, callback){
                        if(data.meeting_id){
                            data.toUpdate = {
                                collectionName: 'meetings', 
                                registryId: data.meeting_id, 
                                data: {
                                    id: postResponse.ops[0]._id.toString(), 
                                    fieldName: 'members', 
                                }                            
                            };
                            self.helper.updateRelationsInDb(data, req, callback);
                        }
                        else{
                            callback({status: 200, response: { 
                                message: 'location was created',
                                success: true,
                                data: {
                                location_id: postResponse.ops[0]._id.toString()
                                }
                            }});  
                        };
                    };
                    data.toUpdate = {
                        collectionName: 'activities', 
                        registryId: data.activity_id, 
                        data: {
                            id: postResponse.ops[0]._id.toString(), 
                            fieldName: 'members', 
                        }                            
                    };
                    self.helper.updateRelationsInDb(data, req, callback, updateRelatedMeetings);
                  }
                  else{
                    callback({status: 200, 
                        response: {success: true, 
                                    message: 'invitation was sent. user not related to any activity/meeting',
                                    data: postResponse.ops[0]
                        }})
                  };
                  //
                }
            });
          }
          else{
            // B - USER/EMAIL DOES NOT EXIST:
              // 1. CREATE USER 
              // 2. SEND INVITATION NOTIFICATION EMAIL
              // 3. ADD USER TO ACTIVITY, ADD USER TO MEETING (IF IDS PROVIDED)
            let dataToRecord = {
                  type: data.type ||'customer' ,
                  name: data.name || '' ,
                  surname: data.surname || '' ,
                  email: data.email || '' ,
                  password: data.password || '' ,
                  username: (data.username!='')?data.username:data.email.slice(0, data.email.indexOf('@')),
                  born_date: data.born_date || '',
                  is_active: true,
                  account_is_confirmed: false,
                  tokens: [],
                  passwordToken: {},
                  profile_pic: data.profile_pic || "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Microsoft_Account.svg/200px-Microsoft_Account.svg.png"
            };
            const userSchema = self.joi.object().keys({
              type: self.joi.string().required(),
              name: self.joi.string().allow('').required(),
              surname: self.joi.string().allow('').required(),
              email: self.joi.string().required(),
              password: self.joi.string().allow('').required(),

              username: self.joi.string().allow(''),
              born_date: self.joi.string().allow(''),
              is_active: self.joi.boolean().required(),
              account_is_confirmed: self.joi.boolean().required(),
              tokens: self.joi.array(), //.allow(''),
              passwordToken: self.joi.object(), //.keys({}), 
              profile_pic: self.joi.string().allow(''),
          });
          try {
              self.joi.assert(dataToRecord, userSchema);
          } catch (e) {
              res.response.error    = { error : { code : "200" , title : "invalid parameter" }};
              res.response.message  = "title" + " " + "invalid parameter";
              return self.callback(res);
          };

          self.store.post({ collection : 'users', data : dataToRecord }, 
            function(error, postResponse){
              if(error){
                res.response.error    = { error : { code : "200" , title :  error.message }};
                res.response.message  = error.message;
                self.callback(res);
              }
              else{
                // SEND INVITATION NOTIFICATION EMAIL
                self.apiMail.sendMail(toPass, function(error, info, callback){
                    if(error){
                      self.callback({status: 409, response: {success: true, message: 'error: invitation was not sent'}})
                    } else {                      
                    //
                      // ADD USER TO ACTIVITY, ADD USER TO MEETING
                      if(data.activity_id){
                        let updateRelatedMeetings = function(data, req, callback){
                            if(data.meeting_id){
                                data.toUpdate = {
                                    collectionName: 'meetings', 
                                    registryId: data.meeting_id, 
                                    data: {
                                        id: postResponse.ops[0]._id.toString(), 
                                        fieldName: 'members', 
                                    }                            
                                };
                                self.helper.updateRelationsInDb(data, req, self.callback);
                            }
                            else{
                              self.callback({status: 200, response: { 
                                    message: 'user was created. invitation was sent. user related an activity. not a meeting',
                                    success: true,
                                    data: {
                                    location_id: postResponse.ops[0]._id.toString()
                                    }
                                }});  
                            };
                        };
                        data.toUpdate = {
                            collectionName: 'activities', 
                            registryId: data.activity_id, 
                            data: {
                                id: postResponse.ops[0]._id.toString(), 
                                fieldName: 'members', 
                            }                            
                        };
                        self.helper.updateRelationsInDb(data, req, self.callback, updateRelatedMeetings);
                      }
                      else{
                        self.callback({status: 200, 
                            response: {success: true, 
                                        message: 'invitation was sent. user not related to any activity/meeting',
                                        data: postResponse.ops[0]
                            }})
                      };
                     //
                    }
                });


              }
          })
        }
      }
    });
    ///
};