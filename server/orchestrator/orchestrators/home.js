module.exports                      = home;

var base                            = require("./../base")

function home(){};
home.prototype                      = new base("home");

home.prototype.init                 = function(data, req, callback){
    this.request                    = req;
    return this.sendPage(data, req, callback);
};

home.prototype.sendPage    = function(data, req, callback){
    var response =  {
        path      : 'index.html'
    };
    callback(response)
};
