module.exports                      = activities;
var base                                = require("./../base")
  // , userstore                          = new (require("./../../persistence/business/userStore"))();
function activities(){
  this.store = require('./../../store/db').instance;
};
activities.prototype                     = new base("activities");
activities.prototype.init = function(data, req, callback){
  this.request = req;
  return this[req.entryMethod](data, req, callback);
};

activities.prototype.getActivities    = function(data, req, callback){
  var filter      = data
  ,   self              = this
  ,   res         = self.restUtils.buildJSONResponse('fail')
  ,   toResponse  = {}
  ,   sort        = filter.sort       || {}
  ,   limit       = filter.limit      || false
  ,   query       = {};

  if(req.query.id!=undefined){
    let idObjectArray = [];
    let idStringArray     = req.query.id.split(',');

    if (idStringArray[0]=='all') {
      query           = {};
    } else {
      idStringArray.forEach(function(itm, idx){
        let objId;
        try {
          objId     = self.store.ObjectId(itm);
        }
        catch(e){
          callback(res)
        }
        idObjectArray.push(objId);
      })  
      query          = { '_id': { $in: idObjectArray } };
    }
  }

  if(req.query.friendlyUrl!=undefined){
    query          = { 'friendlyUrl': req.query.friendlyUrl };
  }

  // query.owner = req.tokenInfo.email;
  query['owner'] = req.tokenInfo['_id'];

  this.store.get({collection: 'activities', query : query, limit : limit , sort : sort}, function(error, dbData){
        if(error){
      // do nothing
        }
        else{
      var hasMatchs           = (dbData.length>0)?true:false;
      if(hasMatchs){
        let result = [];
        dbData.forEach(function(itm, idx){
          result.push({
            "id": dbData[idx]['_id'],
            "title": dbData[idx]['title'],
            "friendlyUrl": dbData[idx]['friendlyUrl'],
            // "template": dbData[idx]['template'],
            // "author_id": dbData[idx]['author_id'],
            // "author": dbData[idx]['author'],
            // "date": dbData[idx]['date'],
            // "description": dbData[idx]['description'],
            // "category": dbData[idx]['category'],
            // "body": dbData[idx]['body'],
            "imgs": dbData[idx]['imgs'],
            "background_color": dbData[idx]['background_color'],
            "owner": dbData[idx]['owner'],
            "tags": dbData[idx]['tags'],
            "members": dbData[idx]['members'],
            "locations": dbData[idx]['locations'],
            "meetings":dbData[idx]['meetings']  
          })
        })
        toResponse          = { data : { items : result, itemsLength: dbData.length} }
        var  interpreter    = new self.interpreter(self.moduleName+"/"+"activities");
        res                       = interpreter.JSON2Response(toResponse)
        res.response.message    = "succesfully returned activities";
      }
      else{
        res.response.error      = { error : { code : "200" , title : "no activities available", detail : "no activities available" } };
        res.response.message    = "no activities available";
      }
        };
    callback(res);
    });
};

activities.prototype.postActivities    = function(data, req, callback){
  var self              = this
  ,   res         = self.restUtils.buildJSONResponse('fail')
  ,   toResponse  = {}
  ,   interpreter   = new self.interpreter(self.moduleName+"/"+"activities")
  ,   dataToInsert = data;
  var dataToRecord = {
      title: data.title || '',
      friendlyUrl: data.friendlyUrl || '',
      date: data.date || '',

      imgs: {
        main: data.imgs.main || '',
      },
      background_color: data.background_color || '#026aa7',

      // template: data.template || '',
      // author_id: data.author_id || '',
      // author: data.author || '',
      // description: data.description || '',
      // category: data.category || '',
      // body: data.body || '',

      owner: data.owner || '',
      tags: data.tags || [],
      members: data.members || [],
      locations: data.tilocationstle || [],
      meetings: data.meetings || [],
  };
  const activitySchema = self.joi.object().keys({
    title: self.joi.string().required(),
    friendlyUrl: self.joi.string().required(),
    date: self.joi.string(),

    imgs:  self.joi.object().keys({
      main: self.joi.string().allow(''),
    }),
    
    background_color: self.joi.string(),
    
    owner: self.joi.string().required(),
    tags: self.joi.array(),
    members: self.joi.array(),
    locations: self.joi.array(),
    meetings: self.joi.array(),
});
try {
    self.joi.assert(dataToRecord, activitySchema);
} catch (e) {
    res.response.error    = { error : { code : "200" , title : "invalid parameter" }};
    res.response.message  = "title" + " " + "invalid parameter";
    return callback(res);
}






  this.store.post({ 'collection' : 'activities', 'data': dataToInsert }, function(error, data){
        if(error){
          // do nothing
        }
        else{
      toResponse            = { data : 'OK'}
      res                     = interpreter.JSON2Response(toResponse)
      res.response.message  = "succesfully inserted activities array";
        };
    callback(res);
    });
};

activities.prototype.putActivities    = function(data, req, callback){
  let self         = this
  ,   res          = self.restUtils.buildJSONResponse('fail');

  let objId;
  try {
      objId        = self.store.ObjectId(data.id);
  }
  catch(e){
      callback(res)
  }

  this.store.put({ 
    collection : 'activities', 
    query : { _id : objId }, 
    update: data.update  }, 
    function(error, dbData){
      if(error){
        res.response.error    = { error : { code : "200" , title :  error.message }};
        res.response.message  = error.message;
        callback(res);
      }
      else{
        callback({status:200, response: {
          success: true,
          data: dbData,
          message: 'activity updated'
        }})
      }
    })
};

activities.prototype.deleteActivities    = function(data, req, callback){
  let self         = this
  ,   res          = self.restUtils.buildJSONResponse('fail');

  let objId;
  try {
      objId     = self.store.ObjectId(data.id);
  }
  catch(e){
      callback(res)
  }

  this.store.put({ 
  collection : 'activities', 
  query : { _id: objId }, 
  update: { is_active: false }  }, 
  function(error, dbData){
      if(error){
      res.response.error    = { error : { code : "200" , title :  error.message }};
      res.response.message  = error.message;
      callback(res);
      }
      else{
      callback({status:200, response: {
          success: true,
          data: dbData
      }})
      }
  })
};


