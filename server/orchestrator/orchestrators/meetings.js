module.exports                      = meetings;
var base                                = require("./../base")
  // , userstore                          = new (require("./../../persistence/business/userStore"))();
function meetings(){
  this.store = require('./../../store/db').instance;
  this.helper = new(require("./../../commons/helper"))();
};
meetings.prototype                     = new base("meetings");
meetings.prototype.init = function(data, req, callback){
  this.request = req;
  return this[req.entryMethod](data, req, callback);
};

meetings.prototype.getMeetings    = function(data, req, callback){
    let self        = this
    ,   res         = self.restUtils.buildJSONResponse('fail')
    ,   toResponse  = {}
    ,   sort        = {} // data.sort       || {}
    ,   limit       = data.limit || false 
    ,   idStringArray     = req.query.id.split(',');
    let query = {};

    if (idStringArray[0]=='all') {
        query           = {};
    } else {
        let idObjectArray = [];
        idStringArray.forEach(function(itm, idx){
            let objId;
            try {
              objId     = self.store.ObjectId(itm);
            }
            catch(e){
              callback(res)
            }
            idObjectArray.push(objId);
        })
    
        query       = { '_id': { $in: idObjectArray } };
    }
    
    self.store.get({collection: 'meetings', query : query, limit : limit , sort : sort}, function(error, dbData){
        if(error){
            // do nothing
        }
        else{
            var hasMatchs           = (dbData.length>0)?true:false;
            if(hasMatchs){
                var result = [];
                dbData.forEach(function(itm, idx){
                  result.push({
                    "id": dbData[idx]['_id'],
                    "title": dbData[idx]['title'],
                    "description":  dbData[idx]['description'],
                    "meeting_number": dbData[idx]['meeting_number'],
                    "location_id": dbData[idx]['location_id'],
                    "date": dbData[idx]['date'],
                    "time_scheduled": dbData[idx]['time_scheduled'],
                    "organizers": dbData[idx]['organizers'],
                    "attendants": dbData[idx]['attendants'],
                    "is_active": dbData[idx]['is_active']
                  })
                })
                toResponse          = { data : { items : result, itemsLength: dbData.length} }
                var  interpreter    = new self.interpreter(self.moduleName+"/"+"meetings");
                res                       = interpreter.JSON2Response(toResponse)
                res.response.message    = "succesfully returned meetings";
            }
            else{
                res.response.error      = { error : { code : "200" , title : "no meetings available", detail : "no meetings available" } };
                res.response.message    = "no meetings available";
      }
        };
        callback(res);
    });
};

meetings.prototype.postMeetings    = function(data, req, callback){
    let self         = this
    ,   res          = self.restUtils.buildJSONResponse('fail');
    
    var dataToRecord = {
        title: data.title || '',
        
        description: data.description || '',
        meeting_number: data.meeting_number || '',
        location_id: data.location_id || '',
        
        date: data.date || '',
        time_scheduled: data.time_scheduled || '',
        
        organizers: data.date || [],
        attendants: data.attendants || [],
        
        is_active : true
    }
    
    // validate schema

    self.store.post({ collection : 'meetings', data : dataToRecord }, 
    function(error, postResponse){
        if(error){
        res.response.error    = { error : { code : "200" , title :  error.message }};
        res.response.message  = error.message;
        callback(res);
        }
        else{
            if(data.activity_id){
                data.toUpdate = {
                    collectionName: 'activities', 
                    registryId: data.activity_id, 
                    data: {
                        id: postResponse.ops[0]._id.toString(), 
                        fieldName: 'meetings', 
                    }                            
                };
                self.helper.updateRelationsInDb(data, req, callback)
            }else{
                callback({status: 200, response: { 
                        message: 'meeting was created',
                        success: true,
                        data: {
                        meeting_id: postResponse.ops[0]._id.toString()
                        }
                    }
                });   
            }
        }
    })
};

meetings.prototype.putMeetings    = function(data, req, callback){
    let self         = this
    ,   res          = self.restUtils.buildJSONResponse('fail');

    let objId;
    try {
        objId     = self.store.ObjectId(data.id);
    }
    catch(e){
        callback(res)
    }

    this.store.put({ 
      collection : 'meetings', 
      query : { _id : objId }, 
      update: data.update  }, 
      function(error, dbData){
        if(error){
          res.response.error    = { error : { code : "200" , title :  error.message }};
          res.response.message  = error.message;
          callback(res);
        }
        else{
          callback({status:200, response: {
            success: true,
            data: dbData,
            message: 'meeting updated'
          }})
        }
      })
};

meetings.prototype.deleteMeetings = function(data, req, callback){
    let self         = this
    ,   res          = self.restUtils.buildJSONResponse('fail');

    let objId;
    try {
        objId     = self.store.ObjectId(data.id);
    }
    catch(e){
        callback(res)
    }

    this.store.put({ 
    collection : 'meetings', 
    query : { _id: objId }, 
    update: { is_active: false }  }, 
    function(error, dbData){
        if(error){
        res.response.error    = { error : { code : "200" , title :  error.message }};
        res.response.message  = error.message;
        callback(res);
        }
        else{
        callback({status:200, response: {
            success: true,
            data: dbData
        }})
        }
    })
};

