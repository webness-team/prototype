module.exports						= orchestrator;

function orchestrator(module){
	this.moduleOrchestrator			= new (require("./orchestrators/"+module))();
};

orchestrator.prototype.init			= function(data, req, callback){
	return this.moduleOrchestrator.init(data, req, callback);
};

orchestrator.prototype.admin		= function(data, req, callback){
	return this.moduleOrchestrator.admin(data, req, callback);
};