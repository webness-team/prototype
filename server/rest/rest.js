/** @class restCore */
module.exports                      = restCore;

var commons                         = require('../commons/commons')
  , modules                         = {
         home                     : require('./accessPoint/accessPoints/home')
       ,  auth                    : require('./accessPoint/accessPoints/auth')
       ,  activities              : require('./accessPoint/accessPoints/activities')
       ,  locations               : require('./accessPoint/accessPoints/locations')
       ,  meetings                : require('./accessPoint/accessPoints/meetings')
       ,  search                  : require('./accessPoint/accessPoints/search')
    // ,  protected               : require('./accessPoint/accessPoints/protected')
    };

/**
 * @constructor restCore
 * @param     {ExpresJs}    express          - ExpressJS.
 * @property  {Router}      this.router      - express.Router instance
 * @property  {Object}      this.utils       - rest object
 * @property  {Object}      this.restUtils   - restUtils instance
 * @property  {Object}      this.secure      - secure instance
 * @property  {Object}      this.httpSecure  - httpSecure instance
*/
function restCore(express){
    this.router                     = express.Router();
    this.utils                      = require("./../commons/utils");
    this.restUtils                  = new (require("./restUtils"))();
    var secureOptions = commons.constants.security
    secureOptions.router = this.router
    this.secure                     = new (require("api-secure"))(secureOptions);
    // this.httpSecure                 = new (require("./../secure/httpSecure"))();
};

/**
 * Set routes based on {@linkcode modules} list.
 * It also set a default 404 page, so it should be call at end
 *
 * @todo refactor: 404 should be separated for manual set if need it
*/
restCore.prototype.setRoutes        = function(){
    for(module in modules){
        new modules[module](this.router).setRoutes();
    }

    var notFound                    = this.restUtils.buildJSONResponse('notFound');
    this.router.all('*', function(req, res){
    // //res.status(notFound.status).json(notFound.response);
    var path                      		= require('path')
    // // ,   filePath = __dirname + '/../public/' + 'notFound.html'
    ,   filePath = __dirname + '/../../public/' + 'index.html'
    res.sendFile(path.resolve(filePath));
    });

};

/**
 * Enable CORS
 *
 * @todo enhace: Allow to recevie cors options (as exposedHeaders)
*/
restCore.prototype.enableCors       = function(){
    var cors                        = require('cors');
    this.router.route('/api/*').all(cors());
};

/**
 * Set extra attributes for each request that handle the server
 *
 * @param {Array} addList - A list of aditional attributes to add in the request, actual valid options: ("startTime", "id")
*/
restCore.prototype.setRequestAttr   = function(addList){
    var self                        = this
      , addList                     = self.utils.ensureArray(addList)
      , fnList                      = {
            "id"                    : () => self.secure.randomValueBase64(self.secure.getRandomInt(4, 8))
          , "startTime"             : () => Date.now()
        };

    this.router.all("/*", function(req, res, next){
        addList.forEach(function(toAdd){
            if(fnList.hasOwnProperty(toAdd)){
                req[toAdd]          = fnList[toAdd]();
            }
        });
        next();
    });
}

/**
 * Use this function for declaring what security components will be up and running.
 *
 * See {@tutorial setSecurity}
*/
restCore.prototype.setSecurity      = function(){
    // this.secure.setSecurityCheck({
    //     accessPoints                : '/*'
    //   , checks                      : ['https']
    // });

    this.secure.setSecurityCheck({
        accessPoints                : [
           {    path: '/api/*', 
                options: { exceptions : [ 

                    { path: '/api/auth/user', method: 'POST'}, 
                    { path: '/api/auth/sign_in', method: 'POST'}, 
                    { path: '/api/auth/user/forgot', method: 'POST'}, 
                    { path:'/api/auth/user/password', method: 'PUT'},

                    { path:'/api/auth/user', method: 'GET'}, 


                    { path:'/api/meetings', method: 'GET'}, 
                    { path:'/api/meetings', method: 'POST'}, 

                    { path:'/api/meetings', method: 'PUT'}, 
                    { path:'/api/meetings', method: 'DELETE'}, 



                    { path:'/api/locations', method: 'GET'}, 
                    { path:'/api/locations', method: 'POST'}, 

                    { path:'/api/locations', method: 'PUT'}, 
                    { path:'/api/locations', method: 'DELETE'}, 

                ] }
            }
        ]
      , checks                      : [{type : 'token'}]
    });


};
