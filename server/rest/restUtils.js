module.exports 						= restUtils;

function restUtils(){

	this.httpCode					= {
		'success'					: 200
	  , 'fail'						: 200
	  , 'unauthorized'				: 401
	  , 'notFound' 					: 404
	  , 'badProtocol' 				: 401
	  , 'notAllowed'				: 405
	  , 'serverError'				: 500
	  , 'internalServerError'		: 500
	  , 'tokenUpdateError'			: 500
	  , 'httpVersionNotSupported' 	: 505
	};

	this.error						= {
		'e0100' : {
			 	title 	: 'invalid http verb'
			,	code 	: 'e0100'
	  		, 	detail	: ''
	  		, 	links 	: {
			   	  self	 	: ''
			 	, related	: 'https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html'
			}
		}
	,	'e0101' : {
			 	title 	: 'http version not supported'
			,	code 	: 'e0101'
	  		, 	detail	: 'the server does not support the HTTP protocol version that was used in your request'
	  		, 	links 	: {
			   	  self	 	: ''
			 	, related	: 'https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.6'
			}
		}
	,	'e0102' : {
			 	title 	: 'unauthorized petition'
			,	code 	: 'e0101'
	  		, 	detail	: 'the server found your credentials to be not valid. therefore access was denied'
	  		, 	links 	: {
			   	  self	 	: ''
			 	, related	: ''
			}
		}
	,	'e0103' : {
			 	title 	: 'content not found'
			,	code 	: 'e0103'
	  		, 	detail	: 'the server has not found anything matching the request-uri'
	  		, 	links 	: {
			   	  self	 	: ''
			 	, related	: 'https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.5'
			}
		}
	,	'e0104' : {
			 	title 	: 'internal server error'
			,	code 	: 'e0104'
	  		, 	detail	: 'the server encountered an unexpected condition which prevented it from fulfilling the request'
	  		, 	links 	: {
			   	  self	 	: ''
			 	, related	: 'https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.1'
			}
		}
	};

	this.defaultResponses 			= {
		'success' : {
			response : {
			   success	: true
			  , data	: {}
			  , message	: 'communication succeeded'
			}
			, status	: this.httpCode.success
		}
		, 'fail' : 	{
			response : {
			   success	: false
			  , error	: {}
			  , message	: 'an unexpected error occurred, please try again later'
			}
			, status	: this.httpCode.fail
		}
		, 'notAllowed' : 	{
			response : {
			   success	: false
			  , error	: {}
			  , message	: 'unauthorized petition'
			}
			, status	: this.httpCode.fail
		}
		, 'badProtocol' : 	{
			response : {
			   success	: false
			  , error	: {}
			  , message	: 'bad protocol'
			}
			, status	: this.httpCode.fail
		}
		, 'httpVersionNotSupported' : 	{
			response : {
			   success	: false
			  , error	: this.error.e0101
			  , message	: 'http protocol not supported, please try again using https'
			}
			, status	: this.httpCode.httpVersionNotSupported
		}
		, 'unauthorized' : 	{
			response : {
			   success	: false
			  , error	: this.error.e0102
			  , message	: 'access denied: failed to authenticate with your credetentials'
			}
			, status	: this.httpCode.unauthorized
		}
		, 'notFound' : 	{
			response : {
			   success	: false
			  , error	: this.error.e0103
			  , message	: 'no content was found for the given request-uri'
			}
			, status	: this.httpCode.notFound
		}
		, 'internalServerError' : 	{
			response : {
			   success	: false
			  , error	: this.error.e0104
			  , message	: 'an internal server error occurred'
			}
			, status	: this.httpCode.internalServerError
		}
	};

};

restUtils.prototype.buildJSONResponse = function(type, info){
	var defaultResponses = this.defaultResponses
	, 	populatedTemplate = defaultResponses[type]
	if(type && info){
		for (var property in info) {
		    if (info.hasOwnProperty(property)) {
		    	if(property=='status'){
			        populatedTemplate.status = info[property];
		    	}
		    	else{
			        populatedTemplate.response[property] = info[property];
		    	}
		    }
		}
	};
	return populatedTemplate;
};

// restUtils.prototype.buildHTMLResponse = function(template, info){
//   	var response 		= {
//   		  template 		: 'index'
//   		 , data 		: {
// 	  		  data : {
// 				  title 	: ''
// 				, message 	: 'This is a dynamically generated html file. It was built using the Pug template engine (former Jade)'
// 				, items 	: [
// 					  { title : "item text number 1" }
// 					, { title : "item text number 2" }
// 					, { title : "item text number 3" }
// 				]
// 	  		}
// 			, metadata 	: {
// 				cssRoutes 	: [
// 					'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
// 				]
// 				, title		: 'IBM Mobile Dev Team'
// 			}
//   		 }
// 	}
// 	return response;
// };
