module.exports                      = search;

var accessPoint                     = require("./../accessPoint");

function search(router){
    this.router                     = router || false;
};

search.prototype                     = new accessPoint("search");

search.prototype.setRoutes           = function(){
    var self                        = this
      , allowed                     = [ "GET" ];

    this.router.route('/api/search/:term?')

        .all(function(){
            self.rejectIfNotAllowed(...arguments, allowed);
        })

        .get(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'find'} );
        })

}
