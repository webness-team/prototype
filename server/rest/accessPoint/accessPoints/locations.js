module.exports                      = locations;

var accessPoint                     = require("./../accessPoint");

function locations(router){
    this.router                     = router || false;
};

locations.prototype                 = new accessPoint("locations");

locations.prototype.setRoutes        = function(){
    var self                        = this
    , allowed                     = [ "GET", "POST", "PUT", "DELETE" ];

    this.router.route('/api/locations/:id?')

        .all(function(){
            self.rejectIfNotAllowed(...arguments, allowed);
        })

        .post(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'postLocations'} );
        })

        .get(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'getLocations'} );
        })

        .put(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'putLocations'} );
        })

        .delete(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'deleteLocations'} );
        });
}
