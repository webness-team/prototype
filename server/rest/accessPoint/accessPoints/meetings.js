module.exports                      = meetings;

var accessPoint                     = require("./../accessPoint");

function meetings(router){
    this.router                     = router || false;
};

meetings.prototype                     = new accessPoint("meetings");

meetings.prototype.setRoutes           = function(){
    var self                        = this
      , allowed                     = [ "GET", "POST", "PUT", "DELETE" ];

    this.router.route('/api/meetings/:id?')

        .all(function(){
            self.rejectIfNotAllowed(...arguments, allowed);
        })

        .post(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'postMeetings'} );
        })

        .get(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'getMeetings'} );
        })

        .put(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'putMeetings'} );
        })

        .delete(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'deleteMeetings'} );
        });
}
