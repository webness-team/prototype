module.exports                      = home;

var accessPoint                     = require("./../accessPoint");

function home(router){
    this.router                     = router || false;
};

home.prototype                      = new accessPoint("home");

home.prototype.setRoutes            = function(){
    var self                        = this
      // , allowed                     = [ "GET", "POST", "PUT" ];

      this.router.route('/')
      // this.router.route('*')

        .all(function(){
            // self.rejectIfNotAllowed(...arguments, allowed);
            // self.orchestratorCall(...arguments);
            self.sendPage(...arguments);
        })

        // .get(function(){
        //     self.orchestratorCall(...arguments);
        // })

        // .post(function(){
        //     self.orchestratorCall(...arguments);
        // });
}
