module.exports						= protected;

var accessPoint 					= require("./../accessPoint");

function protected(router){
	this.router						= router || false;
};

protected.prototype					= new accessPoint("protected");

protected.prototype.setRoutes			= function(){
	var self 						= this
	  , allowed					= [ "GET" ] ;

	this.router.route('/api/protected/test')

		.all(function(){
			self.rejectIfNotAllowed(...arguments, allowed);
		})

		.get(function(){
			self.orchestratorCall(...arguments);
		})

}
