module.exports						= auth;

var accessPoint 					= require("./../accessPoint");

function auth(router){
	this.router						= router || false;
};

auth.prototype						= new accessPoint("auth");

auth.prototype.setRoutes			= function(){
	var self 						= this
	  // , allowed					= [] ;

	this.router.route('/api/auth/user')

		.all(function(){
			self.rejectIfNotAllowed(...arguments, [ "GET", "POST", "PUT", "DELETE" ]);
		})

		.get(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'getUsers'});
		})

		.post(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'createUsers'});
		})

		.put(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'updateUsers'});
		})

		.delete(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'disableUsers'});
		});



	this.router.route('/api/auth/sign_in')

		.all(function(){
			self.rejectIfNotAllowed(...arguments, [ "POST"]);
		})

		.post(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'signIn'});
		});



	this.router.route('/api/auth/user/forgot')

		.all(function(){
			self.rejectIfNotAllowed(...arguments, [ "POST"]);
		})

		.post(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'sendPasswordRecoveryEmail'});
		});



	this.router.route('/api/auth/user/password')

		.all(function(){
			self.rejectIfNotAllowed(...arguments, [ "PUT"]);
		})

		.put(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'updatePassword'});
		});


	
	this.router.route('/api/auth/user/invite')

		.all(function(){
			self.rejectIfNotAllowed(...arguments, [ "POST"]);
		})

		.post(function(){
			self.orchestratorCall(...arguments, {entryMethod: 'inviteNewMembers'});
		});	

}
