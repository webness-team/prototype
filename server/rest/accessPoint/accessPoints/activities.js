module.exports                      = activities;

var accessPoint                     = require("./../accessPoint");

function activities(router){
    this.router                     = router || false;
};

activities.prototype                     = new accessPoint("activities");

activities.prototype.setRoutes           = function(){
    var self                        = this
      , allowed                     = [ "GET", "POST" ];

    this.router.route('/api/activities/:activity_friendly_url?')

        .all(function(){
            self.rejectIfNotAllowed(...arguments, allowed);
        })

        .post(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'postActivities'} );
        })

        .get(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'getActivities'} );
        })

        .put(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'putActivities'} );
        })

        .delete(function(){
            self.orchestratorCall(...arguments,  {entryMethod: 'deleteActivities'} );
        });
}
