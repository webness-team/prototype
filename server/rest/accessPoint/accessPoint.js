module.exports 							= accessPoint;

var logger 								= require("./../../commons/logger");

function accessPoint(moduleName){
	this.moduleName						= moduleName;
	this.utils                      	= require("./../../commons/utils");
	this.restUtils						= new (require("./../../rest/restUtils"))();
	this.orchestrator					= require("./../../orchestrator/orchestrator");
	this.path                      		= require('path');
};

accessPoint.prototype.rejectIfNotAllowed= function(req, res, next, allowed){
	var isAllowed						= (allowed.indexOf(req.method) != -1)?true:false;

	if(isAllowed){
		next();
	}
	else{
		var info 						= { error : this.restUtils.error.e0000, status : this.restUtils.httpCode.notAllowed }
		, 	response					= this.restUtils.buildJSONResponse('notAllowed', info);
		res.status(response.status).json(response);
	}
};

accessPoint.prototype.orchestratorCall	= function(req, res, next, options){
	var data							= (req.method!="GET")?req.body:req.params
	  , orchestrator					= new this.orchestrator(this.moduleName)
	  , entryMethod 					= (options && options.isAdmin)?'admin':'init'
	  , reqId 							= req.id
	  , reqStartTime 					= req.startTime
	  , self							= this
	  , onError 						= function(err){
	  		var error					= err || {name : "", message: ""}
	  		  , name					= error.name || ""
	  		  , msg						= error.message || ""
			  , errorResponse			= self.restUtils.buildJSONResponse('internalServerError')

			logger.msg("rest => accessPoint => orchestratorCall => Service => " + reqId + " - Custom error at: " + req.originalUrl+" - Name: "+name+" - Msg: "+msg, "info");
			if(res.headersSent){
				return;
			}
			else{
				res.status(errorResponse.status).json(errorResponse.response);
			};
	  }
	  , callback						= function(result){
	  		self.utils.tryCatchWrapper(function(){
				var now 				= new Date().getTime()
	  			, 	responseTime  		= (now - reqStartTime)/1000;
	  			logger.msg('Response time => ' + self.moduleName + ' => ' + reqId + ' => : ' + responseTime + ' seconds ', 'info');
				res.status(result.status).json(result.response);
			}, onError);
		};
	
	if(options && options.entryMethod){
		req.entryMethod = options.entryMethod;
	}else{
		req.entryMethod = entryMethod;
	}

	self.utils.tryCatchWrapper(function(){
		orchestrator[entryMethod](data, req, callback);
	}, onError)
};


accessPoint.prototype.sendPage 			= function(req, res, next, isAdmin){
	// res.setHeader('content-type', 'text/html');
	var data							= (req.method!="GET")?req.body:req.params
	  , orchestrator					= new this.orchestrator(this.moduleName)
	  , reqId 							= req.id
	  , reqStartTime 					= req.startTime
	  , entryMethod						= (isAdmin)?"admin":"init"
	  , self							= this
	  , onError 						= function(err){
	  		var error					= err || {name : "", message: ""}
	  		  , name					= error.name || ""
	  		  , msg						= error.message || ""
			  , errorResponse			= self.restUtils.buildJSONResponse('internalServerError')

			logger.msg("rest => accessPoint => sendPage => Service => " + reqId + " - Custom error at: " + req.originalUrl+" - Name: "+name+" - Msg: "+msg, "info");
			if(res.headersSent){
				return;
			}
			else{
				res.status(errorResponse.status).json(errorResponse.response);
			};
	  }
	  , callback						= function(result){
	  		self.utils.tryCatchWrapper(function(){
				var now 				= new Date().getTime()
	  			, 	responseTime  		= (now - reqStartTime)/1000;
	  			logger.msg('Response time => ' + self.moduleName + ' => ' + reqId + ' => : ' + responseTime + ' seconds ', 'info');
				var path = __dirname + '/../../../public/' + result.path
				res.sendFile(self.path.resolve(path));
			}, onError);
		};

	self.utils.tryCatchWrapper(function(){
		orchestrator[entryMethod](data, req, callback);
	}, onError)
};
