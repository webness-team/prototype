var database         = require('api-persistence').mongoApi;

module.exports = {
  db : db
};

function db(dbName){
  this.instance = new (database)(dbName);
};
