module.exports  = userstore
var commons                         = require('./../commons/commons')

function userstore(){
    this.store = require('./db').instance;
    this.secure = new (require('api-secure'))(commons.constants.security);
    this.jwt = require('jsonwebtoken')
}


userstore.prototype.getByEmail        = function(email, onGet){
    var options = { 
        collection : 'users', 
        query : {
            email: email 
        }
    };
    this.store.get(options, onGet)
}




// /**
//  * Guarda o actualiza la información almacenada del usuario
//  * @param {object} user - Objeto con la información del usuario
//  * @param {function} onSave - Callback que maneja el response
//  */
// userstore.prototype.updateUserData  = function(user, onSave){
//     var self                        = this,
//         onGet                       = function(error, info){
//             var result              = []
//             if(error){
//                 // if(error.error == "not_found"){
//                 //     result.push(user)
//                 // }
//                 // else{
//                 //     onSave.apply(self, [error, info])
//                 // }
//             }
//             else{
//                 for(attr in user){
//                     if(attr!=="_rev" && attr!=="_conflicts"){
//                         info[attr]  = user[attr]
//                     }
//                 }

//                 // user.rol            = (info.hasOwnProperty("rol"))?info.rol:"produccion"
//                 // info.rol            = user.rol;

//                 result.push(info)
//             }

//             if(result.length>0){
//                 result              = {"docs": result}
//                 self.store.db.bulk(result, {}, function(errorBulk, resultBulk){
//                     var successMsg  = (errorBulk)?"Update user (bulk) fail":"update user (bulk) success"
//                     self.options.debug.msg(successMsg, "log")
//                     onSave.apply(self, [errorBulk, resultBulk])
//                 })
//             }
//             self.store.put({ 
//                 collection : 'users', 
//                 query : { email: user.email }, 
//                 update: { password: newPassword }  }, 
//                 onGet(error, updateResponse)
//             )
//         }

//     self.store.getByEmail(user.email, onGet)
// }

// /**
//  * Llama a la función updateUserData con la información proporcionada
//  * @param {string|number} dni - Dni correspondiente al usuario del cual se almacenará la información
//  * @param {object} data - Objeto con la información del usuario
//  * @param {function} onSave - Callback que maneja el response
//  */
// userstore.prototype.saveUserData    = function(dni, data, onSave){
//     var self                        = this,
//         onUserUpdate                = function(err, result){
//             if(err){
//                 self.options.debug.msg({name:err.name, error:err.error, reason:err.reason, scope:err.scope, statusCode:err.statusCode}, "log")
//             }
//             else{
//                 onSave.apply(self, [err, result])
//             }
//         }

//     this.updateUserData(data, onUserUpdate)
// }







// // /**
// //  * Actualiza el token en la base de datos
// //  * @param {object} datos - Objeto con la información del token(token actual y documentoLogin)
// //  * @param {boolean} checkDocLogin - Determina si se debe validar el dni
// //  * @param {function} onUpdate - Callback que maneja el response
// //  */
// // userstore.prototype.updateToken     = function(datos, onUpdate, checkDocLogin){
// //     var self                        = this,
// //         token                       = datos.ActualToken,
// //         now                         = new Date().getTime(),
// //         dniLogin                    = datos.documentoLogin,
// //         tokenOrigin = datos.Origen

// //     var onGetByToken                = function(err, data){
// //         if(err){
// //             self.options.debug.msg("userstore => updateToken => onGetByToken -> Error on get token.", "info")
// //             onUpdate(false, {
// //                 status      : 200,
// //                 response    : {
// //                     success : false,
// //                     data    : {},
// //                     message : "Error on get token"
// //                 }
// //             })
// //         }
// //         else{
// //             var tokenArrToSearchIn = (tokenOrigin=="app")?"tokens":"tokensNOL";

// //             if(data.docs.length > 0){
// //                 var user            = data.docs[0],
// //                     isValidToken    = false,
// //                     updateExpire    = false

// //                 user[tokenArrToSearchIn]	     = user[tokenArrToSearchIn].filter(function(itm, idx){
// //                     var inDate      = (itm.expire > now)?true:false,
// //                         isValid     = false

// //                     if(itm.token == token){
// //                         isValid     = (inDate)?true:false
// //                     }

// //                     if(isValid && checkDocLogin && (!dniLogin || (dniLogin && !self.secure.isRequestDataValid(dniLogin, user.docNumber)))){
// //                         isValid     = false
// //                     }

// //                     if(isValid){
// //                         var newToken = self.secure.create({dniLogin: user.docNumber, dniTitular: user.docTitular, rol: user.rolUsuario, origen: tokenOrigin, lastLogin: new Date().getTime() })
// //                         itm.token      = newToken.token
// //                         itm.expire   = newToken.expire
// //                         token          = itm.token
// //                     }

// //                     return isValid
// //                 })

// //                 isValidToken        = (user[tokenArrToSearchIn].length > 0)?true:false

// //                 self.updateUserData(user, function(err, result){
// //                     var baseMsg     = "userstore => updateToken => onGetByToken -> ",
// //                         msg         = (!err)?"Valid token update":("Token update fail: " + err.name + " - " + err.statusCode),
// //                         isError     = (err)?true:false

// //                     self.options.debug.msg(msg, "log")
// //                     onUpdate(isError, {
// //                         status      : (err)?500:200,
// //                         response    : {
// //                             success : (err)?false:true,
// //                             data    : {"token" : token},
// //                             message : msg
// //                         }
// //                     })
// //                 })
// //             }
// //             else {
// //                 self.options.debug.msg("userstore => updateToken => onGetByToken -> No Valid Token.", "info")
// //                 onUpdate(false, {
// //                     status      : 200,
// //                     response    : {
// //                         success : false,
// //                         data    : {},
// //                         message : "No valid token to update"
// //                     }
// //                 })
// //             }
// //         }
// //     }

// //     self.secure.decode(token, function(err, decoded){
// //         if(!err){
// //             self.getByToken(token, onGetByToken)
// //         }
// //         else{
// //             self.options.debug.msg("userstore => decode -> No Valid Token.", "info")
// //             onUpdate(false, {
// //                 status      : 200,
// //                 response    : {
// //                     success : false,
// //                     data    : {},
// //                     message : "No valid token to update was provided"
// //                 }
// //             })
// //         }
// //     }, tokenOrigin)
// // }











// // /**
// //  * Obtiene un documento en base al token proporcionado
// //  * @param {string} token - El token que debe contener el documento
// //  * @param {function} onGet - Callback que maneja el response
// //  */
// // userstore.prototype.getByToken      = function(token, onGet, origen){
// //     var options						= {
// //       selector        : {},
// //       fields						: []
// //   	  , limit						: 1
// //   	};

// //     tokenArrName = (origen=="NOL")?"tokensNOL":"tokens";

// //     options.selector[tokenArrName] =  {
// //           "$elemMatch"		: {
// //             "token"			: token || ""
// //           }
// //         }

// //   	this.store.get(options, onGet)
// // }

// // /**
// //  * Obtiene los documentos cuya propiedad "lastLogin" se encuentre entre las fechas proporcionadas, las mismas inclusives
// //  * @param {number} dateStart - La fecha en timestamp a partir de la cual se buscará coincidencia
// //  * @param {number} endDate - La fecha en timestamp límite hasta la cual se buscará coincidencia
// //  * @param {function} onGet - Callback que maneja el response
// //  */
// // userstore.prototype.getByDate       = function(dateStart, endDate, onGet){
// //     var options                     = {
// //         selector                    : {
// //             "lastLogin"             : {
// //                 "$gte"              : dateStart,
// //                 "$lte"              : endDate
// //             }
// //         },
// //         fields                      : [
// //             "_id",
// //             "lastLogin",
// //             "email"
// //         ]
// //     }
// //     this.store.get(options, onGet)
// // }
























// // /**
// //  * Obtiene un documento en base al dni proporcionado
// //  * @param {string|number} dni - El dni que debe contener el documento
// //  * @param {function} onGet - Callback que maneja el response
// //  */
// // userstore.prototype.getByDNI        = function(dni, onGet){
// //     var options                     = {
// //         selector                    : {
// //             "_id"                   : {
// //                 "$gt"               : 0
// //             },
// //             "docNumber"             : dni || false
// //         },
// //         fields                      : [],
// //         limit                       : 1
// //     }

// //     this.store.get(options, onGet)
// // }