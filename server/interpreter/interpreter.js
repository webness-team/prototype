module.exports 						= interpreter;

var utils							= require("../commons/utils")
, 	logger							= require("../commons/logger");
// var xml2js							= require("xml2js")

function interpreter(dictionary) {
	// utils.testFunction()
	var getModule					= function(modulePath){
		var result					= false;
		try{
			result					= require(modulePath);
		}
		catch(e){
			logger.msg(e.message, 'error');
		}
		return result;
	}

	this.dictionaries				= {
		client2server				: getModule("./client2server/"+dictionary)
	  , server2client				: getModule("./server2client/"+dictionary)
	};

	// this.xml2jsonBuilder			= new xml2js.Builder({
 	//     headless					: true
	//   , allowSurrogateChars		: true
	//   , renderOpts				: {
	// 		newline					: ""
	// 	}
	// });

	// this.json2xmlParser				= new xml2js.Parser({
	// 	explicitArray				: false
	//   , ignoreAttrs					: true
	//   , tagNameProcessors			: [xml2js.processors.stripPrefix]
	// });
}

// interpreter.prototype.testFunction			= function(){
// 	console.log('interpreter test function')
// }

interpreter.prototype.client2server	= function(data){
	return removeProbe(cast(data, this.dictionaries.client2server));
}

interpreter.prototype.server2client	= function(data, dictionary){
	return removeProbe(cast(data, this.dictionaries.server2client));
}

function removeProbe(info){
	// Somehow an attribute gets added to the object (attribute => "__ddProbeAttached__"). It may be related with -> "https://github.com/RuntimeTools/appmetrics/pull/175/files" 
	if(info["__ddProbeAttached__"]){
		delete info["__ddProbeAttached__"];
	}	
	return info;
}

function cast(from, dict){
	var result 						= {};
	for(attr in dict){
		if(typeof dict[attr] == "string"){
			result[attr]			= (dict[attr][0]==="=")?dict[attr].substr(1):utils.getObjVal(from, dict[attr].split("|"));
		}
		else{
			result[attr] 			= cast(from, dict[attr])
		}
	}
	return result;
}

// interpreter.prototype.toSoapXML		= function(jsonToCast){
// 	var xml							= this.xml2jsonBuilder.buildObject(jsonToCast);
// 		xml							= xml.replace(/&quot;/ig,"");
// 	return xml;
// }

interpreter.prototype.toJSONResponse= function(xml){
	var jsonObject 					= {}
	  , self						= this;

	this.json2xmlParser.parseString(xml, function (err, result) {
        jsonObject					= self.server2client(result);
    });

    jsonObject.response.success		= (jsonObject.response.success=="OK")?true:false

	if(!jsonObject.response.error || jsonObject.response.error.estatus != "ERR"){
		delete jsonObject.response.error;
	}

    return jsonObject;
}

interpreter.prototype.toJSONRequest	= function(data){
	var jsonObject 					= this.client2server(data)
	
    return jsonObject;
}

interpreter.prototype.toUrlRequest	= function(data){
	var jsonObject 					= this.client2server(data)
	  , keys						= Object.keys(jsonObject)
	  , urlParams					= ""; 
	
	keys.forEach(function(itm, idx){
		var prepend					= (idx == 0)?"?":"&";
		urlParams					+= prepend+itm+"="+jsonObject[itm]
	})

	return urlParams;	
}


interpreter.prototype.toJSON		= function(xml){
	var jsonObject 					= {}
	  , self						= this;

	this.json2xmlParser.parseString(xml, function (err, result) {
        jsonObject					= result;
    });

    return jsonObject;
}

interpreter.prototype.JSON2Response	= function(data){
	logger.printObj("server -> app -> interpreter -> interpreter.js => JSON2Response is Deprecated, You should use JSONtoResponse", "log")
	var self						= this
	  , jsonObject 					= self.server2client(data);

	if(!jsonObject.response.error || jsonObject.response.error.estatus != "ERR"){
		delete jsonObject.response.error;
	}

	jsonObject.response.success		= (jsonObject.response.success=="OK" || jsonObject.response.success=="true")?true:false
    return jsonObject;
}

// interpreter.prototype.JSONtoResponse= function(data, responseType){
// 	var self						= this
// 	  , responseType				= responseType || "typeA"
// 	  , responseTypes				= {
// 	  		typeA					: ["ERR"]
// 	  	  , typeB					: ["500", 500]
// 	  	  , typeC					: ["400", 400, "409", 409, "500", 500]
// 		}
// 	  , jsonObject 					= self.server2client(data)
// 	  , success						= utils.getObjVal(jsonObject, ["response", "success"]);

// 	if(responseTypes[responseType].indexOf(success)!=-1){
// 		jsonObject.response.success	= false
// 	}
// 	else{
// 		jsonObject.response.success	= true
// 		delete jsonObject.response.error;
// 	}

//     return jsonObject;
// }
