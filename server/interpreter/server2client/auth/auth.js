module.exports								= {
	"response"	: {
		"success"	: "=true"
		, "data"	:	"data"
		, "message"	: "=operation was successful"
	}
	, "status"  : "=200"
}
