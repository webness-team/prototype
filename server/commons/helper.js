module.exports                      = helper
var commons                         = require('./commons')
var base                                = require("./../orchestrator/base");

function helper(options){
    var helper                     = options                   || {};
    this.store = require('./../store/db').instance;
    this.userStore = new(require('./../store/userStore'))();
    this.secure = new (require('api-secure'))(commons.constants.security);
    this.apiMail = new(require("./api-mail"))({config: commons.constants.apiMail});
};
helper.prototype = new base("helper");

helper.prototype.updateRelationsInDb    = function(data, req, callback, onSuccess){
    let self         = this
    ,   res          = self.restUtils.buildJSONResponse('fail');
  
    var toUpdate = // [
        {
            collectionName: data.toUpdate.collectionName, //'',
            registryId: data.toUpdate.registryId, // '',
            data: {
                id: data.toUpdate.data.id, // '', 
                fieldName: data.toUpdate.data.fieldName, // '',
            }
        } // ];
    var toPass = {};
    toPass[data.toUpdate.data.fieldName] = { '$each': [ data.toUpdate.data.id ]  }

    self.store.db.collection(toUpdate.collectionName).updateMany({ _id : self.store.ObjectId(data.toUpdate.registryId) }, 
        { '$push': toPass } 
        , 
        {'upsert': true},
        function(err, updateResponse) {
            if(err){
                res.response.error    = { error : { code : "200" , title :  error.message }};
                res.response.message  = error.message;
                callback(res);
            } else {
                if(onSuccess){
                    onSuccess(data, req, callback)
                }
                else{
                    callback({status: 200, response: { 
                        message: toUpdate.collectionName + ' item was updated with new ' + data.toUpdate.data.fieldName,
                        success: true,
                        data: {
                            registryId: data.toUpdate.registryId
                        }
                    }});      
                }
            }
        }
    );  


}

