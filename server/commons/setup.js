function moduleExport(){
    module.exports                  = {
        devMode                     : devMode
      , constants                   : constants
      , environment                 : environment
      , serverPath                  : serverPath
    };
};

var VCAP_SERVICES                   = (process.env.VCAP_SERVICES)?JSON.parse(process.env.VCAP_SERVICES) : false
  , fs                              = require("fs")
  , devMode                         = true
  , constants                       = {
        logLvl                      : 2 /* 0 => None - 1 => Error & Warning - 2 => Info - 3 => Logger */
      , isLocal                     : (VCAP_SERVICES)?false:true
      , servicesInfo                : {
            mongoLab                : {
                url                 : "mongodb://santidesimone1990:rickandmorty2000@ds157654.mlab.com:57654/webness-prototype"
            }
        }
      // , security                    : {
      //       privateKey              : fs.readFileSync(process.cwd()+"/server/certs/private.key")
      //     , validationPhrase        : "it is not the strongest of the species that survives, nor the most intelligent, but the one most responsive to change"
      //   }
    }
  , environment                     = "Dev"
  , serverPath                      = ""

/* ------------------ */
moduleExport();
/* ------------------ */
