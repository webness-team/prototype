/** @module logger */
var commons                         = require("./commons")
  , constants                       = commons.constants
  , fs                              = require('fs');

module.exports                      = {
    msg                             : msg
  , printObj                        : printObj
};

/**
* Print an object using {@linkcode console.dir} 
* @param {Object} obj     - Object to print.
* @param {Object} options - Options of print, depth and logLvl.
* @example
* printObj({foo:bar}, {depth:null, logLvl:3});
*/
function printObj(obj, options){
    var options                     = options || {};

    options.depth                   = options.depth || null;
    options.logLvl                  = options.logLvl || 3;

    if(constants.logLvl >= options.logLvl){
        console.dir(obj, { depth: options.depth })
    }
}

/**
* Print an object using {@linkcode console.log} || {@linkcode console.info} || {@linkcode console.warn} || {@linkcode console.error}
* only if the server logLvl is >= to type level ({@linkcode { "warn" : 1, "error" : 1, "info" : 2, "log" : 3 }})
* @param {Object} msg     - message to print
* @param {String} type    - type of log to use [log, info, warn, error]
* @example
* // Same as console.info("Hello World")
* msg("Hello World", "info");
*/
function msg(msg, type){
    var logLvl  = { "warn" : 1, "error" : 1, "info" : 2, "log" : 3 }
    if(constants.logLvl >= logLvl[type]){
        console[type](msg)
    }
}

/**
* Write a file with the data 
* @param {String} data        - message to write
* @param {String} targetName  - Name of the file, if not it will take the timestamp
* @example
* write("Hello World", "MyLogFile");
*/
function write(data, targetName){
    var targetName                  = targetName || (new Date().getTime())
      , targetDir                   = process.cwd()+"/server/logs/"
      , targetPath                  = targetDir+targetName+".log"
      , data                        = data || "";

    fs.writeFile(targetPath, data, {flag : 'w'}, function(err){
        if (err){
            msg("logger => write -> Log file creation fail: "+targetName+"", "log")
        } 
        else{
            msg("logger => write -> Log file create: "+targetName+"", "log")
        }
    });
}