/** @module utils */
function moduleExport(){
    module.exports                  = {
        getObjVal                   : getObjVal
      , getType                     : getType
      , base64Decode                : base64Decode
      , tryCatchWrapper             : tryCatchWrapper
      , ensureArray                 : ensureArray
    };
};

/**
* Get the nested value of an object by a given path
* @param {Object} obj           - Root object
* @param {String} path          - Path to the nested value, could be a String or Array.
* @param {String} separator     - Value to make a split over the "path" string (Default '.').
* @example
* getObjVal({foo:{bar:1}}, "foo.bar", ".");
*/
function getObjVal(obj, path, separator){
    var result                      = undefined
      , separator                   = separator || "."
      , path                        = path || ""
      , pathArr                     = (path.constructor === Array)?path:path.split(separator)
      , reduceFn                    = function(prev, act){
            return (prev)?prev[act]:undefined;
        };

    return pathArr.reduce(reduceFn, obj);
};

/**
* Get the type of a given element, always as lowercase
* @param {*} obj                - Element to find the type
* @example
* getObjVal(new Date());
* "date"
*/
function getType(obj){
    var result                      = false;

    if([null, undefined].indexOf(obj)==-1){
        result                      = obj.constructor.toString().toLowerCase().match(/\s([a-z]*)/)[1];
    }
    else{
        result                      = ""+obj;
    }
    return result;
};

/**
* Get the value of a Base64 string
* @param {String} param         - String to be cast
* @param {Boolean} isJson       - Define if should be cast as a JSON or String
* @example
* base64Decode("eyJhIjoiYiJ9")
* '{"a":"b"}'
* base64Decode("eyJhIjoiYiJ9", true)
* {a: "b"}
*/
function base64Decode(param, isJson){
    var base64                      = new Buffer(param, 'base64').toString()
      , result                      = (isJson)?(JSON.parse(base64)):base64;
    return result;
};

/**
* Convenient method that handle a possible error with try/catch
* @param {Function} toExecute   - Code to be handle
* @param {Function} onError     - What to do on error
*/
function tryCatchWrapper(toExecute, onError){
    try{
        toExecute();
    }
    catch(e){
        onError(e);
    }
};

/**
* Force the transformation to an Array
* @param {*} toEnsure           - Anything to be enclosed into an Array
*/
function ensureArray(toEnsure){
    if(toEnsure.constructor!=Array){
        toEnsure        = [toEnsure]
    }
    return toEnsure;
};

/* ------------------ */
moduleExport();
/* ------------------ */
