/** @module commons */
var setup                           = require("./setup")
  , fs                              = require("fs")
  , utils                           = require("./utils")
  , servicesInfo                    = setup.constants.servicesInfo;

function moduleExport(){
    module.exports                  = {
        constants                   : constants
    };
}

var VCAP_SERVICES                   = (process.env.VCAP_SERVICES)?JSON.parse(process.env.VCAP_SERVICES) : false
  , constants                       = {
        logLvl                      : setup.constants.logLvl  //utils.getObjVal(process, "env.logLvl") 
      , devMode                     : setup.devMode // utils.getObjVal(process, "env.devMode") || 
      , isLocal                     : setup.constants.isLocal || false
      , servicesInfo                : {
          mongoLab                : {
               // url                  : (VCAP_SERVICES)?VCAP_SERVICES["mongoLab"][0].credentials.url : servicesInfo.mongoLab.url
               url                  : servicesInfo.mongoLab.url
            }
        }
      , security                    : {
          jwt: { 
              cert: fs.readFileSync(process.cwd()+"/certs/private.key") || setup.constants.security.privateKey
            , expireTime : 360 // 6 hours 
          },
          phrase: "it is not the strongest of the species that survives, nor the most intelligent, but the one most responsive to change" || setup.constants.security.validationPhrase,
          algorithm: "aes256",
          plainEncoding: "utf8",
          encryptEncoding: "urlBase64",
        }
      , apiMail                       : {
            service: 'gmail',
            auth: {
              user: 'noreply.webness',
              pass: 'allyouneedislove'
            }
      }
    }
  , environment                     = setup.environment //utils.getObjVal(process, "env.environment") ||
  , serverPath                      = setup.serverPath //utils.getObjVal(process, "env.serverPath") ||

/* ------------------ */
moduleExport();
/* ------------------ */