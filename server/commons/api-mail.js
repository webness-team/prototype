/*
 * Api-Mail
 *
 */

module.exports                      = apiMail

const nodemailer                    = require('nodemailer'),
    _jade                           = require('jade'),
    path                            = require('path'),
    fs                              = require('fs'),
    defaults                        = {
        service: '',
        auth: {
            user: '',
            pass: ''
        }
        ,
        mailOptions: {
            from: '',
            to: '',
            subject: '',
            html: ''
        }
    };


function apiMail(options){
    var options                     = options                   || {};
    this.config                  = options.config            || undefined;
}


apiMail.prototype.sendMail    = function(mailData, onResponse){
    var templateFileName = mailData.template || 'default';
    var template = path.join(process.cwd(), './templates/'+templateFileName+'.jade'),
        self = this;

    const transporter = nodemailer.createTransport({
        service: self.config.service,
        auth: {
          user: self.config.auth.user,
          pass: self.config.auth.pass
        }
    })

    // transporter.sendMail(mailData, callback)

    fs.readFile(template, 'utf8', function(err, file){
        if(err){
          //handle errors
        //   return res.send('ERROR!');
            return err
        }
        else {
            //compile jade template into function
            var compiledTmpl = _jade.compile(file, {filename: template});
            // set context to be used in template
            var context = mailData.context;
            // get html back as a string with the context applied;
            var html = compiledTmpl(context);

            var data = {
                from: mailData.from, 
                to: mailData.to, 
                subject: mailData.subject, 
                html: html
            }
            transporter.sendMail(data, onResponse)
        }
    });
}

