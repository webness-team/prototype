const express = require('express');
const cfenv = require('cfenv');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');

const restModule    = require('./server/rest/rest');
const rest          = new restModule(express);

// app.use(express.static(__dirname + '/public'));
app.use('/public', express.static(path.join(process.cwd() + '/public')));
app.enable('trust proxy');

rest.setSecurity();
rest.enableCors();
rest.setRequestAttr(["id", "startTime"]);
rest.setRoutes();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

const mongoDbInstance = new require('./server/store/db').db('webness-prototype')

app.use(rest.router);

const appEnv = cfenv.getAppEnv();

app.listen(appEnv.port, '0.0.0.0', function() {
  console.log("server starting on " + appEnv.url);
});
